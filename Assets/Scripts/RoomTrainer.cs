﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTrainer : MonoBehaviour
{
    public int explorerSpeed;
    public int luckySpeed;
    public int staminaSpeed;
    public int smellSpeed;
    public int intelligenceSpeed;

    public float trainingTime = 30f;
    public float particleRate = 1f;

    public RoomDoge targetDoge;
    private float elapsedTime = 0f;
    private float particleTimer = 0f;

    public Transform trainingPosition;
    public Transform finishedPosition;

    public void Update()
    {
        //If doge target is not null, update room trainer
        if (targetDoge != null)
        {
            elapsedTime += Time.deltaTime;
            particleTimer += Time.deltaTime;

            if (elapsedTime >= trainingTime)
            {
                //Complete training
                targetDoge.explorer.CurrentValue += explorerSpeed;
                targetDoge.lucky.CurrentValue += luckySpeed;
                targetDoge.stamina.CurrentValue += staminaSpeed;
                targetDoge.smell.CurrentValue += smellSpeed;
                targetDoge.intelligence.CurrentValue += intelligenceSpeed;

                elapsedTime = 0f;
                particleTimer = 0f;

                targetDoge.transform.position = finishedPosition.position;
                targetDoge.state = RoomDogeState.Idle;
                targetDoge.myCollider.enabled = true;

                targetDoge = null;
            }

            if (particleTimer >= particleRate)
            {
                particleTimer = 0f;
                ParticleManager.instance.GenerateParticle("StatParticle", trainingPosition.position + new Vector3(0f, 0.4f, 0f));
            }
        }
    }
}
