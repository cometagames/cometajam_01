﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ExpeditionManager : MonoBehaviour
{
    public ExpeditionData[] possibleExpeditions;

    public ExpeditionData currentExpedition;
    public float startTime;

    public void StartExpedition()
    {
        int randomN = Random.Range(0, possibleExpeditions.Length);
        currentExpedition = possibleExpeditions[randomN];
        startTime = Time.time;
        InExpedition = true;
    }

    public int earnedFootprints;
    public int earnedGifts;
    public float ElapsedTime;
    public float Remainingtime;
    public bool InExpedition = false;

    public string ExpeditionLog
    {
        get
        {
            return _expeditionLog.ToString();
        }
    }
    private StringBuilder _expeditionLog = new StringBuilder();

    public static ExpeditionManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

    float updateRate = 0.25f;
    float elapsedTimeSinceLastUpdate = 0f;

    private void Update()
    {
        if (currentExpedition != null)
        {
            elapsedTimeSinceLastUpdate += Time.deltaTime;
            if (elapsedTimeSinceLastUpdate >= updateRate)
            {
                UpdateExpedition();
                elapsedTimeSinceLastUpdate = 0f;
            }
        }
    }

    private void UpdateExpedition()
    {
        if (InExpedition)
        {
            earnedFootprints = 0;
            earnedGifts = 0;

            ElapsedTime = Time.time - startTime;
            Remainingtime = Mathf.Clamp(currentExpedition.expeditionDuration - ElapsedTime, 0f, currentExpedition.expeditionDuration);

            //Update log
            _expeditionLog.Clear();

            if (currentExpedition.expeditionEvents.Length > 0)
            {
                foreach (ExpeditionEvent expeditionEvent in currentExpedition.expeditionEvents)
                {
                    if (ElapsedTime >= expeditionEvent.elapsedSeconds)
                    {
                        _expeditionLog.Append(expeditionEvent.elapsedSeconds+": ");
                        _expeditionLog.Append(expeditionEvent.description);

                        if (expeditionEvent.reward.count > 0)
                        {
                            _expeditionLog.Append(". +"+expeditionEvent.reward.count+" "+expeditionEvent.reward.type.ToString());

                            if (expeditionEvent.reward.type == RewardType.Footprint)
                            {
                                earnedFootprints += expeditionEvent.reward.count;
                            } else if (expeditionEvent.reward.type == RewardType.Gift)
                            {
                                earnedGifts += expeditionEvent.reward.count;
                            }
                        }
                        _expeditionLog.Append("\n");
                    }
                }
            }

            //No remaining time
            if (Remainingtime <= 0)
            {
                InExpedition = false;
            }
        } else
        {
            ElapsedTime = 0f;
            Remainingtime = 0f;
        }
    }
}