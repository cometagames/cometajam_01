﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="ExpeditionData", fileName = "ExpeditionData")]
public class ExpeditionData : ScriptableObject
{
    public float expeditionDuration = 46;
    public ExpeditionEvent[] expeditionEvents;
}

[System.Serializable]
public struct ExpeditionEvent
{
    public int elapsedSeconds;
    public string description;
    public ExeditionReward reward;
}

[System.Serializable]
public struct ExeditionReward
{
    public int count;
    public RewardType type;
}

public enum RewardType
{
    Footprint,
    Gift
}