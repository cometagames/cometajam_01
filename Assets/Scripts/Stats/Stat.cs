﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{
    [SerializeField]
	protected float _min;
    [SerializeField]
	protected float _max;
    [SerializeField]
    protected float _currentValue;

    public float MinValue
    {
        get
        {
            return _min;
        }
    }
    public float MaxValue
    {
        get
        {
            return _max;
        }
    }
    public float CurrentValue
    {
        get
        {
            return _currentValue;
        }

        set
        {
            _currentValue = value;
        }
    }
    public Stat(float min, float max, float value)
    {
        _min = min;
        _max = max;
        _currentValue = value;
    }
}
