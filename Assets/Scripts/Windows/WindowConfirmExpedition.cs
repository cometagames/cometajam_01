﻿using CometaFramework;

public class WindowConfirmExpedition : MenuWindow
{

    public void StartExpedition()
    {
        //ExpeditionManager.Instance.StartExpedition();
        MenuController.Instance.GoBack();
        MenuController.Instance.ShowWindow(WindowID.Backpack);
        //SceneManager.LoadScene("SceneWill");
    }

    public void CancelExpedition()
    {
        MenuController.Instance.GoBack();
        GameTiles.instance.ResetTiles();
    }

    public override WindowID ID
    {
        get
        {
            return WindowID.ConfirmExpedition;
        }
    }
}
