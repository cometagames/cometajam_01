﻿using CometaFramework;
using UnityEngine.SceneManagement;

public class WindowSelectDoge : MenuWindow
{

    public void StartExpedition()
    {
        SceneManager.LoadScene("WalkMinigame");
        MenuController.Instance.GoBack();
    }

    public override WindowID ID
    {
        get
        {
            return WindowID.SelectDogePopup;
        }
    }
}
