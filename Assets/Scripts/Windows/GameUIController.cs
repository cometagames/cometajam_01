﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIController : MonoBehaviour
{
    public GameObject checkExpeditionButton;
    public Text expeditionTimer;

    private void Update()
    {
        if (ExpeditionManager.Instance != null &&
            ExpeditionManager.Instance.InExpedition)
        {
            checkExpeditionButton.SetActive(true);
            expeditionTimer.text = ""+(int)ExpeditionManager.Instance.Remainingtime;
        } else
        {
            checkExpeditionButton.SetActive(false);
        }
    }
}
