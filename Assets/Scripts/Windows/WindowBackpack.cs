﻿using CometaFramework;
using UnityEngine.SceneManagement;

public class WindowBackpack : MenuWindow
{
    public override WindowID ID
    {
        get
        {
            return WindowID.Backpack;
        }
    }

    public void AcceptBackpack()
    {
        MenuController.Instance.GoBack();
        ExpeditionManager.Instance.StartExpedition();
        SceneManager.LoadScene("SceneWill");
    }
}
