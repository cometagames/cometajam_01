﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CometaFramework;
using UnityEngine.UI;
using System;

public class WindowExpeditionLog : MenuWindow
{
    public Text remainingTimeText;
    public Text earnedFootprintText;
    public Text earnedGiftsText;

    public ScrollRect logScroll;
    public Text logText;

    protected override void Awake()
    {
        base.Awake();

        this.OnStartShowAnimation += UpdateUI;
    }

    private void UpdateUI()
    {
        if (!ExpeditionManager.Instance.InExpedition)
            return;

        string newText = ExpeditionManager.Instance.ExpeditionLog;
        if (!logText.text.Equals(newText))
        {
            logText.text = ExpeditionManager.Instance.ExpeditionLog;
            logScroll.normalizedPosition = new Vector2(0, 0);
        }

        remainingTimeText.text = ""+(int)ExpeditionManager.Instance.Remainingtime+" seconds";
        earnedFootprintText.text = ""+ExpeditionManager.Instance.earnedFootprints;
        earnedGiftsText.text = ""+ExpeditionManager.Instance.earnedGifts ;
    }

    private void Update()
    {
        UpdateUI();
    }

    public override WindowID ID
    {
        get
        {
            return WindowID.ExpeditionDogeLogPopup;
        }
    }
}
