﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

[RequireComponent(typeof(LeanSelectable))]
public class RoomDoge : LeanSelectableBehaviour
{
    protected override void OnSelect(LeanFinger finger)
    {
        selectFinger = finger;
        startFingerTime = Time.time;
        startPosition = transform.position;
    }
    protected override void OnSelectUp(LeanFinger finger)
    {
        //If dragging, stop dragging
        if (state == RoomDogeState.Dragging)
        {
            mySelectable.Deselect();
            if (detectedTrainer == null)
            {
                state = RoomDogeState.Idle;
            }
            else
            {
                detectedTrainer.targetDoge = this;
                this.transform.position = detectedTrainer.trainingPosition.position;
                state = RoomDogeState.Training;
                myCollider.enabled = false;
            }
        }

        //If select up and is not dragging, select object
        if (state == RoomDogeState.Idle &&
            mySelectable.IsSelected &&
            Time.time <= startFingerTime + LeanTouch.CurrentTapThreshold)
        {
            state = RoomDogeState.Selected;
            //On selection show popup
            MenuController.Instance.ShowWindow(WindowID.SelectDogePopup).OnFinishHideAnimation += OnClosedSelectionPopup;
        }
    }

    protected override void OnDeselect()
    {
        if (state == RoomDogeState.Selected)
        {
            state = RoomDogeState.Idle;
        }
        selectFinger = null;
    }

    
    private void Update()
    {
        //Start dragging
        if (state == RoomDogeState.Idle &&
            mySelectable.IsSelected &&
            Time.time > startFingerTime + LeanTouch.CurrentTapThreshold)
        {
            state = RoomDogeState.Dragging;
        }

        //State dragging
        if (state == RoomDogeState.Dragging)
        {
            Vector2 draggedDistance = selectFinger.ScreenPosition - selectFinger.StartScreenPosition;
            followFinger(draggedDistance);
        }
    }

    private void followFinger(Vector2 distance)
    {
        Camera camera = LeanTouch.GetCamera(null);

        if (camera != null)
        {
            Vector3 targetScreenPosition = camera.WorldToScreenPoint(startPosition);
            targetScreenPosition += (Vector3)distance;
            Vector3 targetWorldPosition = camera.ScreenToWorldPoint(targetScreenPosition);
            transform.position = targetWorldPosition;
        }
    }

    protected void OnClosedSelectionPopup()
    {
        MenuController.Instance.GetWindowByID(WindowID.SelectDogePopup).OnFinishHideAnimation -= OnClosedSelectionPopup;
        mySelectable.Deselect();
        state = RoomDogeState.Idle;
    }

    private void FixedUpdate()
    {
        //If dragging, detect targetable
        if (state == RoomDogeState.Dragging)
        {
            DetectTargetableArount();
        }    
    }

    public RoomTrainer detectedTrainer;
    private void DetectTargetableArount()
    {
        Collider2D otherDetected = Physics2D.OverlapCircle(selectFinger.GetWorldPosition(0), 0.2f, detectLayermask);
        if (otherDetected != null)
        {
            detectedTrainer = otherDetected.GetComponentInParent<RoomTrainer>();
            //Si ya se esta usando, no considear
            if (detectedTrainer.targetDoge != null)
            {
                detectedTrainer = null;
            }
        }
        else
        {
            detectedTrainer = null;
        }
    }

    protected LeanSelectable mySelectable
    {
        get
        {
            if (_myLeanSelectable == null)
            {
                _myLeanSelectable = GetComponent<LeanSelectable>();
            }
            return _myLeanSelectable;
        }
    }
    private LeanSelectable _myLeanSelectable;

    public RoomDogeState state = RoomDogeState.Idle;
    public LayerMask detectLayermask;

    private LeanFinger selectFinger;
    private float startFingerTime;
    private Vector3 startPosition;
    public Collider2D myCollider;

    //Stats
    public Stat explorer = new Stat(0, 10, 0);
    public Stat intelligence = new Stat(0, 10, 0);
    public Stat stamina = new Stat(0, 10, 0);
    public Stat lucky = new Stat(0, 10, 0);
    public Stat smell = new Stat(0, 10, 0);

    public Stat food = new Stat(0, 10, 10);
    public Stat cleaning = new Stat(0, 10, 10);
    public Stat happiness = new Stat(0, 10, 10);
}

public enum RoomDogeState
{
    Idle,
    Dragging,
    Selected,
    Training
}
