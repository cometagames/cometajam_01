﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PrizeTiles : MonoBehaviour
{
    public Tilemap prizeTileMap;
    public Tile prizeTile;
    public int maxPrizes = 2;

    public List<WorldTile> selectedTiles = new List<WorldTile>();
    private List<int> selectedIndex = new List<int>();

    void Start()
    {
        SelectRandomIndex();
        SetPrizesTiles();
    }

    private void SetPrizesTiles()
    {
        int index = 0;
        foreach (Vector3Int pos in prizeTileMap.cellBounds.allPositionsWithin)
        {
            var localPlace = new Vector3Int(pos.x, pos.y, pos.z);
            if (!prizeTileMap.HasTile(localPlace)) continue;
            if (selectedIndex.Contains(index))
            {
                var tile = new WorldTile
                {
                    LocalPlace = localPlace,
                    WorldLocation = prizeTileMap.CellToWorld(localPlace),
                    TileBase = prizeTileMap.GetTile(localPlace),
                    TilemapMember = prizeTileMap,
                    Name = localPlace.x + "," + localPlace.y,
                    Cost = 1
                };
                selectedTiles.Add(tile);
            }
            else
            {
                prizeTileMap.SetTile(localPlace, null);
            }
            index++;
        }
    }

    private void SelectRandomIndex()
    {
        int totalTiles = GetTotalTiles();
        for (int i = 0; i <= maxPrizes; i++)
        {
            int randomIndex = Random.Range(0, totalTiles);
            while (!selectedIndex.Contains(randomIndex))
            {
                selectedIndex.Add(randomIndex);
            }
            if (selectedIndex.Count == maxPrizes)
            {
                break;
            }
        }
    }

    private int GetTotalTiles()
    {
        int totalTiles = 0;
        BoundsInt bounds = prizeTileMap.cellBounds;
        TileBase[] allTiles = prizeTileMap.GetTilesBlock(bounds);
        for (int x = 0; x < bounds.size.x; x++)
        {
            for (int y = 0; y < bounds.size.y; y++)
            {
                TileBase tile = allTiles[x + y * bounds.size.x];
                if (tile != null)
                {
                    totalTiles++;
                }
            }
        }
        return totalTiles;
    }

    public bool IsTileInPrizePosition(WorldTile tile)
    {
        foreach (WorldTile _tile in selectedTiles)
        {
            if (_tile.LocalPlace == tile.LocalPlace)
            {
                return true;
            }
        }
        return false;
    }
}
