﻿using UnityEngine;
using System.Collections.Generic;
using Lean.Touch;

public class TileManager : MonoBehaviour
{
    public TIle startingPoint;
    public List<TIle> allTiles;

    private TIle currentTile;
    private bool isDragging = false;

    void Start()
    {
        AddEvents(); 
    }

    private void AddEvents()
    {
        foreach (TIle tile in allTiles)
        {
            if (!tile.isObstacle)
            {
                tile.Select += OnSelectTile;
                tile.Deselect += OnDeselectTile;
                tile.Hover += OnHoverTile;
            }
        }
    }

    private void OnSelectTile(TIle tile)
    {
        if (tile.isObstacle) return;
        if (tile.isStartingPoint || tile.isOccupied)
        {
            isDragging = true;
        }
    }

    private void OnHoverTile(TIle tile)
    {
        if (tile.isObstacle) return;
        currentTile = tile;
        if (isDragging && !tile.isOccupied && !tile.isStartingPoint)
        {
            tile.ActivateTile();
        }
    }

    private void OnDeselectTile(TIle tile)
    {
        isDragging = false;
    }
}
