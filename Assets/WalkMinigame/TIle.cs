﻿using Lean.Touch;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TIle : MonoBehaviour
{
    public bool isOccupied = false;
    public bool isObstacle = false;
    public bool isStartingPoint = false;

    public delegate void TileEvent(TIle tile);
    public event TileEvent Select;
    public event TileEvent Hover;
    public event TileEvent Deselect;

    private Image tileImage;
    private EventTrigger eventTrigger;
    private List<TIle> adjacentTiles;
    [HideInInspector]
    public LeanSelectable leanSelectable;

    void Awake()
    {
        tileImage = GetComponent<Image>();
        leanSelectable = GetComponent<LeanSelectable>();
        eventTrigger = GetComponent<EventTrigger>();
        leanSelectable.DeselectOnUp = true;

        leanSelectable.OnSelect.AddListener(OnSelectFinger);
        leanSelectable.OnDeselect.AddListener(OnDeselect);

        AddHoverEvent();

        if (isStartingPoint)
        {
            tileImage.color = Color.green;
        }

        if (isObstacle)
        {
            tileImage.color = Color.red;
        }
    }

    private void AddHoverEvent()
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerEnter;
        entry.callback.AddListener((data) => { OnPointerEnterDelegate((PointerEventData)data); });
        eventTrigger.triggers.Add(entry);
    }

    public void ActivateTile()
    {
        isOccupied = true;
        tileImage.color = Color.yellow;
    }

    private void OnSelectFinger(LeanFinger finger)
    {
        Select?.Invoke(this);
    }

    public void OnDeselect()
    {
        Deselect?.Invoke(this);
    }

    public void OnPointerEnterDelegate(PointerEventData data)
    {
        Hover?.Invoke(this);
    }
}
