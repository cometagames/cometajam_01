﻿using Lean.Touch;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using CometaFramework;

public class GameTiles : MonoBehaviour
{
    public float maximumMovements = 5f;
    public PrizeTiles prizeTiles;
    public static GameTiles instance;
    //Tile al que vamos a cambiar cuando seleccionen el camino
    public Tile walkableTile;
    //Tile original si se resetea el camino
    public Tile originalTile;
    //Tile map que vamos a checar el camino
    public Tilemap tileMap;
    //Diccionario de información de todos los tiles a los que se pueden caminar
    public Dictionary<Vector3, WorldTile> tiles;

    //Evento cuando ya no te quedan movmientos o terminaste de construir
    public delegate void OnMovementsFinished();
    public event OnMovementsFinished MovementsFinished;

    //Posición actual del input (dedo) al tile que está seleccionando
    private Vector3 fingerPosition = Vector3.zero;
    //Valor de referencia del tile que vamos a checar si existe en el diccionario
    private WorldTile _tile;
    //Celda actual
    private Vector3Int currentCell;
    //Celda que estamos seleccionando
    private Vector3Int movingCell;
    private List<WorldTile> currentPath = new List<WorldTile>();

    [HideInInspector]
    public float tempMaxMovements = 0;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        //else if (instance != this)
        //{
        //    Destroy(gameObject);
        //}

        GetWorldTiles();
    }

    void Start()
    {
        tempMaxMovements = maximumMovements;
        LeanTouch.OnFingerSet += OnFingerSet;
        currentCell = tileMap.WorldToCell(fingerPosition);
    }

    private void OnFingerSet(LeanFinger finger)
    {
        fingerPosition = finger.GetWorldPosition(10);
        if (fingerPosition == Vector3.zero || tempMaxMovements <= 0) return;
        movingCell = tileMap.WorldToCell(fingerPosition);

        if (tileMap.HasTile(movingCell))
        {
            // set the new tile
            Vector3 currentCellPosition = tileMap.CellToWorld(currentCell);
            Vector3 movingCellPosition = tileMap.CellToWorld(movingCell);
            if (tiles.TryGetValue(currentCellPosition, out _tile))
            {
                //Si el tile al que apuntamos es vecino del actual, nos podemos mover
                if (_tile.IsTileNeighbor(movingCell))
                {
                    //Si llegamos al tile inicial terminamos el camino
                    if (tiles[movingCellPosition].StartingPoint)
                    {
                        tempMaxMovements = 0;
                        MovementsFinished?.Invoke();
                        currentCell = movingCell;
                        CheckForPrizes();
                        MenuController.Instance.ShowWindow(WindowID.ConfirmExpedition);
                        return;
                    }
                    tileMap.SetTile(movingCell, walkableTile);
                    currentCell = movingCell;
                    tempMaxMovements--;
                    currentPath.Add(tiles[movingCell]);
                    if (tempMaxMovements <= 0)
                    {
                        currentCell = tiles[Vector2.zero].LocalPlace;
                        ResetTiles();
                        MovementsFinished?.Invoke();
                    }
                }
            }
        }
    }

    public void ResetTiles()
    {
        tempMaxMovements = maximumMovements;
        foreach (KeyValuePair<Vector3, WorldTile> worldTile in tiles)
        {
            tileMap.SetTile(worldTile.Value.LocalPlace, originalTile);
        }
        currentPath.Clear();
    }

    private void CheckForPrizes()
    {
        int prizesSelected = 0;
        foreach (WorldTile tile in currentPath)
        {
            if (prizeTiles.IsTileInPrizePosition(tile))
            {
                prizesSelected++;
            }
        }
        Debug.Log("Prizes selected: " + prizesSelected);
    }

    private void GetWorldTiles()
    {
        tiles = new Dictionary<Vector3, WorldTile>();
        foreach (Vector3Int pos in tileMap.cellBounds.allPositionsWithin)
        {
            var localPlace = new Vector3Int(pos.x, pos.y, pos.z);

            if (!tileMap.HasTile(localPlace)) continue;
            var tile = new WorldTile
            {
                LocalPlace = localPlace,
                WorldLocation = tileMap.CellToWorld(localPlace),
                TileBase = tileMap.GetTile(localPlace),
                TilemapMember = tileMap,
                Name = localPlace.x + "," + localPlace.y,
                Cost = 1,
                neighbors = AddNeighbors(localPlace),
            };
            tile.StartingPoint = IsStartingPoint(tile);
            tiles.Add(tile.WorldLocation, tile);
        }
    }

    private List<Vector3Int> AddNeighbors(Vector3Int currentTile)
    {
        List<Vector3Int> adjacentTiles = new List<Vector3Int>();
        //Checar a los lados
        if (tileMap.HasTile(new Vector3Int(currentTile.x + 1, currentTile.y, currentTile.z)))
        {
            adjacentTiles.Add(new Vector3Int(currentTile.x + 1, currentTile.y, currentTile.z));
        }

        if (tileMap.HasTile(new Vector3Int(currentTile.x - 1, currentTile.y, currentTile.z)))
        {
            adjacentTiles.Add(new Vector3Int(currentTile.x - 1, currentTile.y, currentTile.z));
        }

        //Checar arriba y abajo
        if (tileMap.HasTile(new Vector3Int(currentTile.x, currentTile.y + 1, currentTile.z)))
        {
            adjacentTiles.Add(new Vector3Int(currentTile.x, currentTile.y + 1, currentTile.z));
        }

        if (tileMap.HasTile(new Vector3Int(currentTile.x, currentTile.y - 1, currentTile.z)))
        {
            adjacentTiles.Add(new Vector3Int(currentTile.x, currentTile.y - 1, currentTile.z));
        }
        return adjacentTiles;
    }

    private bool IsStartingPoint(WorldTile tile)
    {
        return (tile.LocalPlace.x == 0 && tile.LocalPlace.y == 0);
    }
}
