﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

//Clase para almacenar información de los tiles
public class WorldTile
{
    public Vector3Int LocalPlace { get; set; }

    public Vector3 WorldLocation { get; set; }

    public TileBase TileBase { get; set; }

    public Tilemap TilemapMember { get; set; }

    public string Name { get; set; }

    // Below is needed for Breadth First Searching
    public bool IsExplored { get; set; }

    public WorldTile ExploredFrom { get; set; }

    public int Cost { get; set; }

    public List<Vector3Int> neighbors = new List<Vector3Int>();

    public bool StartingPoint { get; set; }

    public bool Occupied { get; set; }

    public bool IsTileNeighbor(Vector3Int tileToTest)
    {
        return neighbors.Contains(tileToTest);
    }
}