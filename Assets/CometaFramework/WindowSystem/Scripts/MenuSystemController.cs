﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CometaFramework
{
    public class MenuSystemController : MonoBehaviour
    {
        #region public_methods
        public MenuWindow ShowWindow(MenuWindow window, WindowTransitionType transitionType)
        {
            if (window == null)
            {
                Debug.LogError("Cant show a null window");
                return null;
            }

            //If we call show a showed window, do nothing
            if (CurrentWindow == window)
            {
                return null;
            }

            //Since this is not a child window, hide all other windows before
            if (CurrentWindow != null)
            {
                CloseAll();
            }

            //Show new window
            if (transitionType == WindowTransitionType.Default) transitionType = showTransitionType;
            window.Show(showAnimationTime, transitionType);
            windowsHistory.Push(window);

            lastInputTime = Time.unscaledTime;
            return window;
        }

        public MenuWindow ShowSubWindow(MenuWindow subWindow, WindowTransitionType transitionType, bool hidePreviousWindow = false)
        {
            if (subWindow == null)
            {
                Debug.LogWarning("Attepting to show null window");
                return null;
            }

            if (hidePreviousWindow)
            {
                CurrentWindow.Hide(hideAnimationTime, CurrentWindow.myEntryAnimation);
            }

            //Show sub window
            if (transitionType == WindowTransitionType.Default) transitionType = showTransitionType;
            subWindow.Show(showAnimationTime, transitionType);
            windowsHistory.Push(subWindow);

            lastInputTime = Time.unscaledTime;
            return subWindow;
        }

        //Returns to previous screen in the history
        public void GoBack()
        {
            //If we have no current window, just exit
            if (CurrentWindow == null)
                return;

            MenuWindow _currentWindow = CurrentWindow;
            windowsHistory.Pop();
            MenuWindow _previousWindow = CurrentWindow;

            _currentWindow.Hide(hideAnimationTime, _currentWindow.myEntryAnimation,
                () =>
                {
                    if (CurrentWindow != null && CurrentWindow.ID == _previousWindow.ID)
                    {
                        CurrentWindow.Show(showAnimationTime, CurrentWindow.myEntryAnimation);
                    }
                });
            lastInputTime = Time.unscaledTime;
        }

        public void CloseAll()
        {
            if (windowsHistory.Count == 0)
                return;

            while (windowsHistory.Count > 0)
            {
                MenuWindow currentWindow = windowsHistory.Pop();
                currentWindow.Hide(hideAnimationTime, currentWindow.myEntryAnimation);
            }

            windowsHistory.Clear();
            lastInputTime = Time.unscaledTime;
        }

        public bool IsShowingWindow(MenuWindow menuWindow)
        {
            return windowsHistory.Contains(menuWindow);
        }
        #endregion

        #region private_methods
        #endregion

        #region public_vars
        public MenuWindow CurrentWindow
        {
            get { return windowsHistory.Count == 0 ? null : windowsHistory.Peek(); }
        }

        public bool CanHandleInput
        {
            get
            {
                if (Time.unscaledTime > lastInputTime + inputDelay)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion

        #region private_vars
        [SerializeField]
        private float showAnimationTime = 0.3f;
        [SerializeField]
        private float hideAnimationTime = 0.1f;
        [SerializeField]
        private WindowTransitionType showTransitionType = WindowTransitionType.ScaleChange;

        [SerializeField]
        private float inputDelay = 0.3f;
        private float lastInputTime = 0;

        private Stack<MenuWindow> windowsHistory = new Stack<MenuWindow>();
        #endregion
    }
}