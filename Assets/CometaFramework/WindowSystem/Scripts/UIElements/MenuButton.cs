﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CometaFramework
{
    public class MenuButton : UIButton
    {
        public WindowID windowID;
        public bool isSubWindow = false;

        protected override void HandleButtonClick()
        {
            if (isSubWindow)
            {
                MenuController.Instance.ShowSubWindow(windowID);
            } else
            {
                MenuController.Instance.ShowWindow(windowID);
            }
        }
    }
}
