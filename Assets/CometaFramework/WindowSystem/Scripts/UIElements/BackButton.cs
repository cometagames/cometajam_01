﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CometaFramework {
    public class BackButton : UIButton
    {
        protected override void HandleButtonClick()
        {
            MenuController.Instance.GoBack();
        }
    }
}
