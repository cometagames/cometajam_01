﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CometaFramework
{
    [RequireComponent(typeof(Button))]
    public abstract class UIButton : MonoBehaviour
    {
        public void OnButtonClick()
        {
            HandleButtonClick();
        }
        protected abstract void HandleButtonClick();
    }
}