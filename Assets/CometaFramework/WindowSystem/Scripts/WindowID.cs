using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WindowID
{
    None = 0,
    QuitGamePopup = 1,
    SelectDogePopup = 2,
    ExpeditionDogeLogPopup = 3,
    ExpeditionConfigPopup = 4,
    ConfirmExpedition = 5,
    Backpack = 6
}
