using UnityEngine;
using System.Collections.Generic;
using CometaFramework;

[RequireComponent(typeof(MenuSystemController))]
public class MenuController : MonoBehaviour
{
    #region public_methods
    //Show window with reference
    public MenuWindow ShowWindow(MenuWindow screenToShow, WindowTransitionType transitionType = WindowTransitionType.Default)
    {
        return MenuSystem.ShowWindow(screenToShow, transitionType);
    }

    //Show window with id
    public MenuWindow ShowWindow(WindowID windowID, WindowTransitionType transitionType = WindowTransitionType.Default)
    {
       return ShowWindow(GetWindowByID(windowID), transitionType);
    }

    //Show sub window, based on screen id
    public MenuWindow ShowSubWindow(MenuWindow screenToShow, WindowTransitionType transitionType = WindowTransitionType.Default, bool hidePreviousWindow = false)
    {
        return MenuSystem.ShowSubWindow(screenToShow, transitionType, hidePreviousWindow);
    }

    //Show sub window, based on screen id
    public MenuWindow ShowSubWindow(WindowID windowID, WindowTransitionType transitionType = WindowTransitionType.Default, bool hidePreviousWindow = false)
    {
        return ShowSubWindow(GetWindowByID(windowID), transitionType, hidePreviousWindow);
    }

    public MenuWindow GetWindowByID(WindowID windowID)
    {
        Debug.Assert(menuWindowsDictionary.ContainsKey(windowID), "Windows dictionary does not contains id: "+windowID);
        return menuWindowsDictionary[windowID];
    }

    public bool IsWindowShowing(WindowID windowID)
    {
        return MenuSystem.IsShowingWindow(GetWindowByID(windowID));
    }

    public void GoBack()
    {
        MenuSystem.GoBack();
    }
    #endregion

    #region private_methods
    private void Awake()
    {
        Instance = this;
        Debug.Assert(menuWindowsDictionary != null, "Dictionary for windows should not be null");
        //For each window, add listener, add to dictionary and activate gameobject
        foreach (MenuWindow menuWindow in menuWindows)
        {
            menuWindow.OnStartShowAnimation += HandleOnStartShowWindow;
            menuWindow.OnFinishShowAnimation += HandleOnFinishShowWindow;
            menuWindow.OnStartHideAnimation += HandleOnStartHideWindow;
            menuWindow.OnFinishHideAnimation += HandleOnFinishHideWindow;

            menuWindowsDictionary.Add(menuWindow.ID, menuWindow);

            if (!menuWindow.gameObject.activeSelf)
            {
                menuWindow.gameObject.SetActive(true);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //If current window is null, exit of game
            if (MenuSystem.CurrentWindow == null)
            {
                ShowWindow(WindowID.QuitGamePopup);
            } else
            {
                MenuSystem.GoBack();
            }
        }
    }

    private void HandleOnStartShowWindow()
    {
        OnStartShowWindow?.Invoke();
    }
    private void HandleOnFinishShowWindow()
    {
        OnFinishShowWindow?.Invoke();
    }
    private void HandleOnStartHideWindow()
    {
        OnStartHideWindow?.Invoke();
    }
    private void HandleOnFinishHideWindow()
    {
        OnFinishHideWindow?.Invoke();
    }
    #endregion

    #region public_vars
    public static MenuController Instance;

    public delegate void MenuControllerEvent();
    public event MenuControllerEvent OnStartShowWindow;
    public event MenuControllerEvent OnFinishShowWindow;
    public event MenuControllerEvent OnStartHideWindow;
    public event MenuControllerEvent OnFinishHideWindow;
    #endregion

    #region private_vars
    [SerializeField]
    public MenuWindow[] menuWindows;

    private Dictionary<WindowID, MenuWindow> menuWindowsDictionary = new Dictionary<WindowID, MenuWindow>();

    private MenuSystemController _menuSystem;
    private MenuSystemController MenuSystem
    {
        get
        {
            if (_menuSystem == null)
            {
                _menuSystem = GetComponent<MenuSystemController>();
            }
            return _menuSystem;
        }
    }
    #endregion
}
