﻿
public enum WindowTransitionType
{
    Default,
    ScaleChange,
    FromBottomPosition
}