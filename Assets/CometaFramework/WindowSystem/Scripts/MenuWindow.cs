using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace CometaFramework
{
    [RequireComponent(typeof(CanvasGroup))]
    public class MenuWindow : MonoBehaviour
    {
        #region public_methods
        public virtual void Show(float animationTime, WindowTransitionType animationType, System.Action OnComplete = null)
        {
            if (!gameObject.activeSelf)
                gameObject.SetActive(true);
            myCanvas.enabled = true;
            DOTween.Kill(gameObject);

            ShowAnimation(animationTime, animationType, OnComplete);

            // set our default UI item so it has focus.
            myCanvasGroup.interactable = true;
        }

        public virtual void Hide(float animationTime, WindowTransitionType animationType, System.Action OnComplete = null)
        {
            // cancel any running tweens in case we are leaving this menu immediately after having opened it.
            DOTween.Kill(gameObject);

            HideAnimation(animationTime, animationType, OnComplete);

            myCanvasGroup.interactable = false;
        }
        #endregion

        #region private_methods
        private void ShowAnimation(float animationTime, WindowTransitionType animationType, System.Action OnComplete = null)
        {
            OnStartShowAnimation?.Invoke();
            myEntryAnimation = animationType;

            if (backgroundCanvasGroup != null)
            {
                if (animationTime > 0)
                {
                    backgroundCanvasGroup.DOFade(backgroundTargetAlpha, animationTime).SetUpdate(true);
                }
                else
                {
                    backgroundCanvasGroup.alpha = backgroundTargetAlpha;
                }
            }

            switch (animationType)
            {
                case WindowTransitionType.ScaleChange:

                    if (animationTime > 0)
                    {
                        transform.DOScale(Vector3.one, animationTime).SetEase(Ease.OutBack).SetUpdate(true).OnComplete(()=> { HandleCompleteShowAnimation(); OnComplete?.Invoke(); });
                    }
                    else
                    {
                        transform.localScale = Vector3.one;
                        myCanvas.enabled = true;
                        HandleCompleteShowAnimation();
                        OnComplete?.Invoke();
                    }
                    break;
                case WindowTransitionType.FromBottomPosition:
                    transform.localScale = Vector3.one;
                    transform.localPosition = windowHiddenPosition;

                    if (animationTime > 0)
                    {
                        transform.DOLocalMove(windowShowingPosition, animationTime).SetEase(Ease.OutBack).SetUpdate(true).OnComplete(()=> { HandleCompleteShowAnimation(); OnComplete?.Invoke(); });
                    }
                    else
                    {
                        transform.localPosition = windowShowingPosition;
                        myCanvas.enabled = true;
                        HandleCompleteShowAnimation();
                        OnComplete?.Invoke();
                    }
                    break;
                case WindowTransitionType.Default:
                default:
                    Debug.LogError("ANIMATE WITH ANIMATION **Default**, NOTHING TO DO HERE");
                    break;
            }
        }

        private void HideAnimation(float animationTime, WindowTransitionType animationType, System.Action OnComplete = null)
        {
            OnStartHideAnimation?.Invoke();

            //If we have background animation
            if (backgroundCanvasGroup != null)
            {
                if (animationTime > 0)
                {
                    backgroundCanvasGroup.DOFade(0, animationTime).SetUpdate(true);
                }
                else
                {
                    backgroundCanvasGroup.alpha = 0;
                }
            }

            switch (animationType)
            {
                case WindowTransitionType.ScaleChange:
                    if (animationTime > 0)
                    {
                        transform.DOScale(Vector3.zero, animationTime).SetEase(Ease.InBack).SetUpdate(true).OnComplete(()=> { HandleCompleteHideAnimation(); OnComplete?.Invoke(); });
                    }
                    else
                    {
                        transform.localScale = Vector3.zero;
                        myCanvas.enabled = false;
                        HandleCompleteHideAnimation();
                        OnComplete?.Invoke();
                    }
                    break;
                case WindowTransitionType.FromBottomPosition:
                    transform.localPosition = windowShowingPosition;

                    if (animationTime > 0)
                    {
                        transform.DOLocalMove(windowHiddenPosition, animationTime).SetEase(Ease.InBack).SetUpdate(true).OnComplete(()=> { HandleCompleteHideAnimation(); OnComplete?.Invoke(); });
                    }
                    else
                    {
                        transform.localPosition = windowHiddenPosition;
                        myCanvas.enabled = false;
                        HandleCompleteHideAnimation();
                        OnComplete?.Invoke();
                    }
                    break;
                default:
                    break;
            }
        }

        private void HandleCompleteShowAnimation()
        {
            OnFinishShowAnimation?.Invoke();
        }
        private void HandleCompleteHideAnimation()
        {
            myCanvas.enabled = false;
            OnFinishHideAnimation?.Invoke();
        }

        //Initialize window
        protected virtual void Awake()
        {
            if (myCanvas == null)
            {
                myCanvas = GetComponent<Canvas>();
            }
            Debug.Assert(myCanvas != null, "Window with id: " + ID + " does not contains canvas reference");
            myCanvas.enabled = false;

            myCanvasGroup = GetComponent<CanvasGroup>();
            Debug.Assert(myCanvasGroup != null, "Window with id: " + ID + " does not contains canvas group reference");
            myCanvasGroup.interactable = false;
            myCanvasGroup.transform.localScale = Vector3.zero;
            windowShowingPosition = transform.localPosition;
            if (backgroundCanvasGroup != null)
            {
                backgroundCanvasGroup.alpha = 0;
            }
        }
        #endregion

        #region public_vars
        public delegate void MenuWindowEvent();
        public event MenuWindowEvent OnStartShowAnimation;
        public event MenuWindowEvent OnFinishShowAnimation;
        public event MenuWindowEvent OnStartHideAnimation;
        public event MenuWindowEvent OnFinishHideAnimation;

        public WindowTransitionType myEntryAnimation;

        public virtual WindowID ID
        {
            get
            {
                Debug.LogError("WindowId property not implementet for: " + gameObject.name);
                return WindowID.None;
            }
        }

        public bool IsCanvasActive
        {
            get { return myCanvas.enabled; }
        }
        #endregion

        #region private_vars
        [SerializeField]
        private Canvas myCanvas;
        private CanvasGroup myCanvasGroup;
        [SerializeField]
        private CanvasGroup backgroundCanvasGroup;
        [SerializeField]
        private float backgroundTargetAlpha = 0.9f;

        private Vector3 windowShowingPosition = Vector3.zero;
        [SerializeField]
        private Vector3 hiddenPositionOffset = new Vector3(0f, -800f, 0f);

        private Vector3 windowHiddenPosition
        {
            get
            {
                return windowShowingPosition + hiddenPositionOffset;
            }
        }
        #endregion
    }
}