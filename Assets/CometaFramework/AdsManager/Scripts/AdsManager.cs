﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsManager : MonoBehaviour
{
    #region public_methods

    #endregion

    #region private_methods
    private void Awake()
    {
        if (isInstantiated)
        {
            return;
        }

        Instance = this;
        //InitializePurchasing();
        isInstantiated = true;
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    #region public_vars
    public static AdsManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject adsManager = new GameObject("AdsManager");
                adsManager.SetActive(false);
                _instance = adsManager.AddComponent<AdsManager>();
                //_instance.config = Resources.Load(InAppPurchasingConfig.AssetName) as AdsConfig
                //InitializePurchasing
                _instance.isInstantiated = true;
                adsManager.SetActive(true);

                DontDestroyOnLoad(adsManager);
            }
            return _instance;
        }
        private set { _instance = value; }
    }
    #endregion

    #region private_vars
    private static AdsManager _instance;

    private bool isInstantiated = false;
    #endregion
}
//public class AdsController : MonoBehaviour
//{
//    public bool isUsingIronSource = true;
//    public static AdsController Instance;

//    public bool CanShowAds
//    {
//        get
//        {
//            if (IsRewardedVideoAvailable)
//            {
//                return true;
//            }
//            else
//            {
//                MenuController.instance.ShowSubScreen(MenuScreenId.NoConnectionUI);
//                return false;
//            }
//        }
//    }
//    public bool IsRewardedVideoAvailable
//    {
//        get
//        {
//            if (IronSource.Agent.isRewardedVideoAvailable())
//            {
//                return true;
//            }

//#if UNITY_EDITOR
//            return true;
//#endif
//            return false;
//        }
//    }

//    public bool CanShowOfferwall
//    {
//        get;
//        private set;
//    }

//    public void Awake()
//    {
//        Instance = this;

//        StartCoroutine(InitIronSource());
//    }

//    IEnumerator InitIronSource()
//    {
//        yield return null;
//        IronSourceConfig.Instance.setClientSideCallbacks(true);

//        IronSource.Agent.validateIntegration();

//        string appKey = (Application.platform == RuntimePlatform.Android) ? "a6077555" : "a4203d95";//Poner valores correctos

//        IronSource.Agent.init(appKey, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.OFFERWALL);

//        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
//        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
//        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
//        IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
//        IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
//        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
//        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;

//        IronSourceEvents.onOfferwallClosedEvent += OfferwallClosedEvent;
//        IronSourceEvents.onOfferwallOpenedEvent += OfferwallOpenedEvent;
//        IronSourceEvents.onOfferwallShowFailedEvent += OfferwallShowFailedEvent;
//        IronSourceEvents.onOfferwallAdCreditedEvent += OfferwallAdCreditedEvent;
//        IronSourceEvents.onGetOfferwallCreditsFailedEvent += GetOfferwallCreditsFailedEvent;
//        IronSourceEvents.onOfferwallAvailableEvent += OfferwallAvailableEvent;

//        IronSource.Agent.getOfferwallCredits();
//    }

//    public void ShowOfferwall()
//    {
//        PlayerPrefs.SetInt("offerwallView", 1);
//        IronSource.Agent.showOfferwall();
//    }

//    public bool AccelerateBatchAd()
//    {
//#if UNITY_EDITOR
//        currentAd = AdType.AccelerateBatch;
//        currentResult = AdResult.Finished;
//        adCallback?.Invoke(currentAd, currentResult);
//        BatchController.Instance.AccelerateWithAd();
//        return true;
//#endif
//        return ShowRewardedAd(AdType.AccelerateBatch);
//    }

//    public void SpinRouletteAD()
//    {
//#if UNITY_EDITOR
//        currentAd = AdType.SpinRoulette;
//        currentResult = AdResult.Finished;
//        adCallback?.Invoke(currentAd, currentResult);
//        RouletteManager.instance.SpinRoulette();
//        return;
//#endif
//        ShowRewardedAd(AdType.SpinRoulette);
//    }

//    public bool SeagullAD()
//    {
//#if UNITY_EDITOR
//        currentAd = AdType.Seagull;
//        currentResult = AdResult.Finished;
//        adCallback?.Invoke(currentAd, currentResult);
//        SeagullController.instance.GiveReward();
//        return true;
//#endif
//        return ShowRewardedAd(AdType.Seagull);
//    }

//    public bool DuplicateRewardAD()
//    {
//#if UNITY_EDITOR
//        currentAd = AdType.Duplicate;
//        currentResult = AdResult.Finished;
//        adCallback?.Invoke(currentAd, currentResult);
//        DiscoveryController.instance.DuplicateReward();
//        return true;
//#endif
//        return ShowRewardedAd(AdType.Duplicate);
//    }

//    public bool ChestAD()
//    {
//#if UNITY_EDITOR
//        currentAd = AdType.Chest;
//        currentResult = AdResult.Finished;
//        adCallback?.Invoke(currentAd, currentResult);
//        ChestController.instance.GetChestReward();
//        return true;
//#endif
//        return ShowRewardedAd(AdType.Chest);
//    }

//    public bool PremiumBusAD()
//    {
//#if UNITY_EDITOR
//        currentAd = AdType.PremiumBus;
//        currentResult = AdResult.Finished;
//        adCallback?.Invoke(currentAd, currentResult);
//        PremiumBusController.instance.ActivatePremiumBus();
//        return true;
//#endif
//        return ShowRewardedAd(AdType.PremiumBus);
//    }

//    private void HandleAcceleratebatchAd(AdResult result)
//    {
//        switch (result)
//        {
//            case AdResult.Finished:
//                BatchController.Instance.AccelerateWithAd();
//                break;
//            case AdResult.Skipped:
//            case AdResult.Failed:
//                Debug.Log("Skip or fail ad");
//                break;
//        }
//    }

//    private void HandleSpinRouletteAD(AdResult result)
//    {
//        switch (result)
//        {
//            case AdResult.Finished:
//                RouletteManager.instance.SpinRoulette();
//                break;
//            case AdResult.Skipped:
//            case AdResult.Failed:
//                Debug.Log("Skip or fail ad");
//                break;

//        }
//    }

//    private void HandleSeagullAD(AdResult result)
//    {
//        switch (result)
//        {
//            case AdResult.Finished:
//                SeagullController.instance.GiveReward();
//                break;
//            case AdResult.Skipped:
//            case AdResult.Failed:
//                Debug.Log("Skip pr fail ad");
//                break;
//        }
//    }

//    private void HandleDuplicateRewardAD(AdResult result)
//    {
//        switch (result)
//        {
//            case AdResult.Finished:
//                DiscoveryController.instance.DuplicateReward();
//                break;
//            case AdResult.Skipped:
//            case AdResult.Failed:
//                Debug.Log("Skip pr fail ad");
//                break;
//        }
//    }

//    private void HandleChestReward(AdResult result)
//    {
//        switch (result)
//        {
//            case AdResult.Finished:
//                ChestController.instance.GetChestReward();
//                break;
//            case AdResult.Skipped:
//            case AdResult.Failed:
//                Debug.Log("Skip pr fail ad");
//                break;
//        }
//    }

//    private void HandlePremiumBusAD(AdResult result)
//    {
//        switch (result)
//        {
//            case AdResult.Finished:
//                PremiumBusController.instance.ActivatePremiumBus();
//                break;
//            case AdResult.Skipped:
//            case AdResult.Failed:
//                Debug.Log("Skip pr fail ad");
//                break;
//        }
//    }

//    /// <summary>
//    /// Shows a rewarded video and returns a callback
//    /// </summary>
//    /// <param name="adType">The ad type to show</param>
//    /// <param name="callback">The callback if necessary</param>
//    public bool ShowRewardedAd(AdType adType, System.Action<AdType, AdResult> callback = null)
//    {
//        //Debug.Log("CALL SHOW REWARDED AD, ADTYPE: " + adType + ", CanShow?: " + CanShowAds);
//        if (CanShowAds)
//        {
//            currentResult = AdResult.None;
//            currentAd = adType;
//            adCallback = callback;
//            IronSource.Agent.showRewardedVideo();
//            return true;
//        }
//        return false;
//    }

//    void ManageAdReward()
//    {
//        //If is waiting for the result or result is none, wait for other event
//        switch (currentAd)
//        {
//            case AdType.AccelerateBatch:
//                HandleAcceleratebatchAd(AdResult.Finished);
//                break;
//            case AdType.SpinRoulette:
//                HandleSpinRouletteAD(AdResult.Finished);
//                break;
//            case AdType.Seagull:
//                HandleSeagullAD(AdResult.Finished);
//                break;
//            case AdType.Duplicate:
//                HandleDuplicateRewardAD(AdResult.Finished);
//                break;
//            case AdType.Chest:
//                HandleChestReward(AdResult.Finished);
//                break;
//            case AdType.PremiumBus:
//                HandlePremiumBusAD(AdResult.Finished);
//                break;
//        }

//        adCallback?.Invoke(currentAd, currentResult);

//        currentAd = AdType.None;
//        currentResult = AdResult.None;
//    }

//    public AdType currentAd = AdType.None;
//    public AdResult currentResult = AdResult.None;
//    public System.Action<AdType, AdResult> adCallback;



//    #region listners
//    //Invoked when the RewardedVideo ad view has opened.
//    //Your Activity will lose focus. Please avoid performing heavy 
//    //tasks till the video ad will be closed.
//    void RewardedVideoAdOpenedEvent()
//    {
//        PauseGame(true);
//    }
//    //Invoked when the RewardedVideo ad view is about to be closed.
//    //Your activity will now regain its focus.
//    void RewardedVideoAdClosedEvent()
//    {
//        PauseGame(false);

//        currentResult = AdResult.Finished;
//        //ManageAdReward();
//    }
//    //Invoked when there is a change in the ad availability status.
//    //@param - available - value will change to true when rewarded videos are available. 
//    //You can then show the video by calling showRewardedVideo().
//    //Value will change to false when no videos are available.
//    void RewardedVideoAvailabilityChangedEvent(bool available)
//    {
//        Debug.Log("AdsController - RewardedVideoAvailabilityChangedEvent");

//    }
//    //Invoked when the video ad starts playing.
//    void RewardedVideoAdStartedEvent()
//    {
//        Debug.Log("AdsController - RewardedVideoAdStartedEvent");

//    }
//    //Invoked when the video ad finishes playing.
//    void RewardedVideoAdEndedEvent()
//    {
//        Debug.Log("AdsController - RewardedVideoAdEndedEvent");

//    }
//    //Invoked when the user completed the video and should be rewarded. 
//    //If using server-to-server callbacks you may ignore this events and wait for 
//    //the callback from the ironSource server.
//    //@param - placement - placement object which contains the reward data
//    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
//    {
//        Debug.Log("AdsController - RewardedVideoAdRewardedEvent");

//        currentResult = AdResult.Finished;
//        ManageAdReward();
//    }
//    //Invoked when the Rewarded Video failed to show
//    //@param description - string - contains information about the failure.
//    void RewardedVideoAdShowFailedEvent(IronSourceError error)
//    {
//        currentResult = AdResult.Failed;
//        //ManageAdReward();
//    }


//    /**
//    * Invoked when there is a change in the Offerwall availability status.
//    * @param - available - value will change to YES when Offerwall are available. 
//    * You can then show the video by calling showOfferwall(). Value will change to NO when Offerwall isn't available.
//    */
//    void OfferwallAvailableEvent(bool canShowOfferwall)
//    {
//        Debug.Log("AdsController - OfferwallAvailableEvent");
//        CanShowOfferwall = canShowOfferwall;

//    }
//    /**
//     * Invoked when the Offerwall successfully loads for the user.
//     */
//    void OfferwallOpenedEvent()
//    {
//        Debug.Log("AdsController - OfferwallOpenedEvent");
//        //PlayerPrefs.SetInt("offerwallView", 1);
//    }
//    /**
//     * Invoked when the method 'showOfferWall' is called and the OfferWall fails to load.  
//    *@param desc - A string which represents the reason of the failure.
//     */
//    void OfferwallShowFailedEvent(IronSourceError error)
//    {
//        Debug.Log("AdsController - OfferwallShowFailedEvent");
//    }
//    /**
//      * Invoked each time the user completes an offer.
//      * Award the user with the credit amount corresponding to the value of the ‘credits’ 
//      * parameter.
//      * @param dict - A dictionary which holds the credits and the total credits.   
//      */
//    void OfferwallAdCreditedEvent(Dictionary<string, object> dict)
//    {

//        Debug.Log("AdsController - OfferwallAdCreditedEvent");
//        Debug.Log("I got OfferwallAdCreditedEvent, current credits = " + dict["credits"] + "totalCredits = " + dict["totalCredits"]);

//        int totalDiamondsRewarded = System.Convert.ToInt32(dict["credits"]);

//        if (totalDiamondsRewarded > 0 && PlayerPrefs.GetInt("offerwallView", 0) == 1)
//        {
//            PlayerPrefs.SetInt("totalDiamondsRewarded", totalDiamondsRewarded);
//            PlayerPrefs.SetInt("offerwallView", 0);
//            StartCoroutine(ShowSuccesfullPopUp());
//        }
//    }

//    IEnumerator ShowSuccesfullPopUp()
//    {
//        Debug.Log(this + "ShowPopUp A");
//        yield return new WaitForSecondsRealtime(2);
//        Debug.Log(this + "ShowPopUp B");
//        ((OfferwallUI)MenuController.instance.GetScreenById(MenuScreenId.OfferwallUI)).IsReward = true;
//        MenuController.instance.ShowScreen(MenuScreenId.OfferwallUI);
//    }

//    /**
//      * Invoked when the method 'getOfferWallCredits' fails to retrieve 
//      * the user's credit balance info.
//      * @param desc -string object that represents the reason of the  failure. 
//      */
//    void GetOfferwallCreditsFailedEvent(IronSourceError error)
//    {
//        Debug.Log("AdsController - GetOfferwallCreditsFailedEvent");
//    }

//    /**
//      * Invoked when the user is about to return to the application after closing 
//      * the Offerwall.
//      */
//    void OfferwallClosedEvent()
//    {
//        Debug.Log("AdsController - OfferwallClosedEvent");
//        Debug.Log("AdsController - Call getOfferwallCredits");
//        IronSource.Agent.getOfferwallCredits();
//    }


//    #endregion

//    /// <summary>
//    /// Pauses the game while showing ad
//    /// </summary>
//    protected void PauseGame(bool pauseGame)
//    {
//        if (pauseGame)
//        {
//            Time.timeScale = 0;
//            AudioListener.pause = true;
//        }
//        else
//        {
//            Time.timeScale = 1;
//            AudioListener.pause = false;
//        }
//    }


//    void OnApplicationPause(bool isPaused)
//    {
//        IronSource.Agent.onApplicationPause(isPaused);
//    }

//}

//public enum AdType
//{
//    None,
//    AccelerateBatch,
//    SpinRoulette,
//    Seagull,
//    Duplicate,
//    Chest,
//    PremiumBus
//}

//public enum AdResult
//{
//    None,
//    WaitingResult,
//    Finished,
//    Skipped,
//    Failed
//}