﻿using System.Collections.Generic;
using System.Numerics;
using System.Text;
using UnityEngine.EventSystems;

namespace CometaFramework.StringExtentions
{
	public static class StringExtentions
    {
		public static string SetColor(this string inputText, string color)
        {
			return "<color="+color+">"+inputText+"</color>";
		}

		public static string SetColor(this string inputText, Colors color)
        {
			return inputText.SetColor (color.ToString());
		}

        public static string ExtededToString(this List<int> list)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("List: { ");
            for (int i=0;i<list.Count;i++)
            {
                builder.Append(list[i].ToString());
                if (i < list.Count - 1)
                {
                    builder.Append(" , ");
                }
                else
                {
                    builder.Append(" ");
                }
            }
            builder.Append("}");
            return builder.ToString();
        }

        public static BigInteger SumValues(this List<int> list)
        {
            BigInteger sum = 0;
            for (int i=0;i<list.Count;i++)
            {
                sum += BigInteger.Pow(2, list[i]);
            }
            return sum;
        }

        public static string ExtendedToString(this List<RaycastResult> raycastResults)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("List: { ");
            for (int i=0;i<raycastResults.Count;i++)
            {
                builder.Append("gameobject: "+raycastResults[i].gameObject.name);
                if (i < raycastResults.Count - 1)
                {
                    builder.Append(" , ");
                }
                else
                {
                    builder.Append(" ");
                }
            }
            builder.Append("}");
            return builder.ToString();
        }
    }

	public enum Colors
    {
		aqua,
		black,
		blue,
		brown,
		cyan,
		darkblue,
		fuchsia,
		green,
		grey,
		lightblue,
		lime,
		magenta,
		maroon,
		navy,
		olive,
		purple,
		red,
		silver,
		teal,
		white,
		yellow
	}
}