﻿using System.Collections.Generic;
using UnityEngine;

namespace Utility.ListExtentions
{
    public static class ListExtentions
    {
        public static void Shuffle<T>(this List<T> entry) where T: Object
        {
            for (int i=0;i< entry.Count;i++)
            {
                int rnd = Random.Range(0, entry.Count);
                T temp = entry[rnd];
                entry[rnd] = entry[i];
                entry[i] = temp;
            }
        }

        public static void Shuffle(this List<int> entry)
        {
            for (int i = 0; i < entry.Count; i++)
            {
                int rnd = Random.Range(0, entry.Count);
                int temp = entry[rnd];
                entry[rnd] = entry[i];
                entry[i] = temp;
            }
        }
    }
}