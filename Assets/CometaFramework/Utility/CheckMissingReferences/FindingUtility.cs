﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FindingUtility : MonoBehaviour {

    public static List<GameObject> FindAllSceneGameobjectsIncludingInactive()
    {
        Scene scene = SceneManager.GetActiveScene();
        GameObject[] rootGameobjects = scene.GetRootGameObjects();

        List<GameObject> gameobjects = new List<GameObject>();
        foreach (GameObject obj in rootGameobjects)
        {
            gameobjects.AddRange(FindGameobjectsInChildren(obj));
        }

        return gameobjects;
    }
    private static List<GameObject> FindGameobjectsInChildren(GameObject parent)
    {
        List<GameObject> gameObjects = new List<GameObject>();
        gameObjects.Add(parent);
        
        //Iteracion para checar a cada uno de los hijos del gameobject
        for (int i=0;i<parent.transform.childCount;i++)
        {
            gameObjects.AddRange(FindGameobjectsInChildren(parent.transform.GetChild(i).gameObject));
        }
        return gameObjects;
    }
}
