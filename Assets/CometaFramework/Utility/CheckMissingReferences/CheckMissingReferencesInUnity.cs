﻿// Based on http://www.tallior.com/find-missing-references-unity/
// It fixes deprecations and checks for missing references every time a new scene is loaded
// Moreover, it inspects missing references in animators and animation frames
#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Linq;

[InitializeOnLoad]
public static class LatestScenes
{
    static string currentScene;
    static LatestScenes()
    {
        EditorApplication.hierarchyChanged += hierarchyWindowChanged;
    }
    static void hierarchyWindowChanged()
    {
        if (currentScene != EditorSceneManager.GetActiveScene().name)
        {
            CheckMissingReferences.FindMissingReferencesInCurrentScene();
            currentScene = EditorSceneManager.GetActiveScene().name;
        }
    }
}

public static class CheckMissingReferences
{

    [MenuItem("Tools/Show Missing Object References in all scenes", false, 52)]
    public static void MissingSpritesInAllScenes()
    {
        foreach (var scene in EditorBuildSettings.scenes.Where(s => s.enabled))
        {
            EditorSceneManager.OpenScene(scene.path);
            var objects = FindAllGameobjects(false);
            FindMissingReferences(scene.path, objects);
        }
    }

    [MenuItem("Tools/Show Missing Object References in scene", false, 50)]
    public static void FindMissingReferencesInCurrentScene()
    {
        var objects = FindAllGameobjects(false);
        FindMissingReferences(EditorSceneManager.GetActiveScene().name, objects);
    }

    [MenuItem("Tools/Show Missing Object References in scene including inactive", false, 51)]
    public static void FindMissingReferencesInCurrentSceneIncludingInactivate()
    {
        var objects = FindAllGameobjects(true);
        FindMissingReferences(EditorSceneManager.GetActiveScene().name, objects);
    }

    [MenuItem("Tools/Show Missing Object References in assets", false, 53)]
    public static void MissingSpritesInAssets()
    {
        var allAssets = AssetDatabase.GetAllAssetPaths();
        var objs = allAssets.Select(a => AssetDatabase.LoadAssetAtPath(a, typeof(GameObject)) as GameObject).Where(a => a != null).ToArray();

        FindMissingReferences("Project", objs);
    }

    public static GameObject[] FindAllGameobjects(bool findInactive)
    {
        if (findInactive)
        {
            return FindingUtility.FindAllSceneGameobjectsIncludingInactive().ToArray();
        } else
        {
            return Object.FindObjectsOfType<GameObject>();
        }
        
    }

    public static void FindMissingReferences(string sceneName, GameObject[] objects)
    {
        foreach (var go in objects)
        {
            var components = go.GetComponents<Component>();

            foreach (var c in components)
            {
                if (c == null)
                {
                    Debug.Log("Found a null in : "+go.name+" in scene: "+sceneName);
                }
                var so = new SerializedObject(c);
                var sp = so.GetIterator();

                while (sp.NextVisible(true))
                {
                    if (sp.propertyType == SerializedPropertyType.ObjectReference)
                    {
                        if (sp.objectReferenceValue == null && sp.objectReferenceInstanceIDValue != 0)
                        {
                            ShowError(FullObjectPath(go), sp.name, sceneName);
                        }
                    }
                }
                var animator = c as Animator;
                if (animator != null)
                {
                    CheckAnimatorReferences(animator);
                }
                var spriteRenderer = c as SpriteRenderer;
                if (spriteRenderer != null)
                {
                    CheckSpriteRendererReferences(spriteRenderer);
                }
                var image = c as UnityEngine.UI.Image;
                if (image != null)
                {
                    CheckImageReferences(image);
                }
            }
        }
    }

    public static void CheckAnimatorReferences(Animator component)
    {
        if (component.runtimeAnimatorController == null)
        {
            return;
        }
        foreach (AnimationClip ac in component.runtimeAnimatorController.animationClips)
        {
            var so = new SerializedObject(ac);
            var sp = so.GetIterator();

            while (sp.NextVisible(true))
            {
                if (sp.propertyType == SerializedPropertyType.ObjectReference)
                {
                    if (sp.objectReferenceValue == null && sp.objectReferenceInstanceIDValue != 0)
                    {
                        Debug.LogError("Missing reference found in: " + FullObjectPath(component.gameObject) + "Animation: " + ac.name + ", Property : " + sp.name +", DisplayName : "+sp.displayName+ ", Scene: " + EditorSceneManager.GetActiveScene().name);
                    }
                }
            }
        }
    }
    public static void CheckSpriteRendererReferences(SpriteRenderer spriteRenderer)
    {
        Sprite referencedSprite = spriteRenderer.sprite;
        if (referencedSprite == null)
        {
            try
            {
                string spriteName = referencedSprite.name;
            }
            catch (MissingReferenceException)
            {
                Debug.LogError("Missing or null sprite found in: " + FullObjectPath(spriteRenderer.gameObject));
            }
            catch (System.Exception e)
            {
                Debug.LogError("Exeption for sprite: "+ FullObjectPath(spriteRenderer.gameObject)+", type: "+e.GetType().FullName);
            }
        }
    }

    public static void CheckImageReferences(UnityEngine.UI.Image image)
    {
        Sprite referencedSprite = image.sprite;
        //If referenceSprite, not equals a null, there is a reference then
        if (!ReferenceEquals(referencedSprite, null))
        {
            try
            {
                string spriteName = referencedSprite.name;
            }
            catch(MissingReferenceException)
            {
                Debug.LogError("Missing or null sprite found in: " + FullObjectPath(image.gameObject));
            }
            catch (System.Exception)
            {

            }
        }
    }

    static void ShowError(string objectName, string propertyName, string sceneName)
    {
        Debug.LogError("Missing reference found in: " + objectName + ", Property : " + propertyName + ", Scene: " + sceneName);
    }

    static string FullObjectPath(GameObject go)
    {
        return go.transform.parent == null ? go.name : FullObjectPath(go.transform.parent.gameObject) + "/" + go.name;
    }
}
#endif