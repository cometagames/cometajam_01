﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MainCameraRef : Singleton<MainCameraRef>
{
    [SerializeField]
    private new Camera camera;
    public Camera canvasCamera;
    public Camera MainCamera
    {
        get
        {
            if (camera == null)
            {
                camera = GetComponent<Camera>();
            }
            return camera;
        }
    }

    public LayerMask defaultGameplayLayermask;
    public LayerMask buildingLayermask;
}
