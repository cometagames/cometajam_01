﻿using UnityEngine;

[ExecuteInEditMode]
public class AnchorTransform : MonoBehaviour
{
    public Vector2 targetViewportPoint;
    public Vector2 offset;

    public bool setHorizontalPosition = false;
    public bool setVerticalPosition = false;

    void SetAnchor()
    {
        Vector3 worldPoint = cameraRef.ViewportToWorldPoint(targetViewportPoint);
        Vector3 anchoredPosition = transform.position;

        if (setHorizontalPosition)
        {
            anchoredPosition.x = worldPoint.x + offset.x; ;
        }

        if (setVerticalPosition)
        {
            anchoredPosition.y = worldPoint.y + offset.y;
        }

        transform.position = anchoredPosition;
    }

    // Set Anchor at Start

    void Start()
    {
        SetAnchor();
    }

    Camera _camera;
    Camera cameraRef
    {
        get
        {
            if (_camera == null)
            {
                _camera = Camera.main;
            }
            return _camera;
        }
    }

#if UNITY_EDITOR
    // This only update the function in the editor. This portion won't be compiled into the target build.
    // Reference: http://docs.unity3d.com/Documentation/Manual/PlatformDependentCompilation.html
    // Feel free to remove this (or comment it out) if you do not need to see it updating in Scene View
    public bool viewOnUpdate = true;
    void Update()
    {
        if (viewOnUpdate)
        {
            this.SetAnchor();
        }

    }

#endif
}
