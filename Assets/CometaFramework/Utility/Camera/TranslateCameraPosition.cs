﻿using UnityEngine;

[ExecuteInEditMode]

public class TranslateCameraPosition : MonoBehaviour
{
    public Transform fromTransformReference;
    public Camera fromCamera;

    public Camera toCamera;
    public bool viewOnUpdate = true;

    void TranslatePosition()
    {
        if (fromTransformReference == null || fromCamera == null || toCamera == null)
        {
            return;
        }
        Vector3 translatedPosition;
        Vector2 fromScreenPosition = fromCamera.WorldToScreenPoint(fromTransformReference.position);
        //Vector2 fromScreenPosition = RectTransformUtility.WorldToScreenPoint(fromCamera, fromTransformReference.position);
        translatedPosition = toCamera.ScreenToWorldPoint(new Vector3(fromScreenPosition.x, fromScreenPosition.y, 10f));

        transform.position = translatedPosition;
    }

    private void Start()
    {
        TranslatePosition();
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (viewOnUpdate)
        {
            TranslatePosition();
        }
    }
#endif
}
