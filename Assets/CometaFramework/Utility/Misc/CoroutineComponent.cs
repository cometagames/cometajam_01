﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CometaFramework
{
	/// <summary>
	/// This script will be used just for StartCoroutine, not anything else
	/// </summary>
	public class CoroutineComponent : MonoBehaviour
	{
	}
}
