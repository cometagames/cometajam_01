﻿using System;
using System.Collections.Generic;
using System.Linq;
using rds;
using UnityEngine;
using CometaFramework;

public class Rewards
{
    public static RewardObject GetReward(List<RewardObject> rewardsList)
    {
        RDSTable rewardTable = new RDSTable
        {
            rdsCount = 1
        };

        foreach (RewardObject reward in rewardsList)
        {
            reward.rdsProbability = reward.probability;
            rewardTable.AddEntry(reward);
        }

        return (RewardObject)rewardTable.rdsResult.First();
    }
}

[Serializable]
public class RewardObject : RDSObject
{
    public RewardsType rewardType;
    public float probability;
    public Vector2 minMaxReward;
    [HideInInspector]
    public int rewardAmount = 0;

    public RewardObject()
    {
        rewardType = RewardsType.None;
        probability = 0;
        rewardAmount = 0;
        minMaxReward = Vector2.zero;
    }

    public void GetRandomRewardAmount(float minValue, float maxValue)
    {
        rewardAmount = (int)UnityEngine.Random.Range(minValue, maxValue + 1);
    }

    public void GetRandomRewardAmount()
    {
        rewardAmount = (int)UnityEngine.Random.Range(minMaxReward.x, minMaxReward.y);
    }
}
