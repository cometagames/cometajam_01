﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CometaFramework
{
    [CreateAssetMenu(menuName = "CometaFramework/AnalyticsConfig", fileName = AssetName)]
    public class AnalyticsConfig : ScriptableObject
    {
        #region public_methods
        #endregion

        #region private_methods
        #endregion

        #region public_vars
        public const string AssetName = "AnalyticsConfig";

        public bool ShowEditorLog = true;
        #endregion

        #region private_vars
        #endregion
    }
}
