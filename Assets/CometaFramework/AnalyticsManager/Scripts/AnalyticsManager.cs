﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using CometaFramework.StringExtentions;

namespace CometaFramework
{
    public class AnalyticsManager : MonoBehaviour
    {
		#region public_methods
		#endregion

		#region private_methods
		private void Awake()
		{
			if (Initialized)
			{
				return;
			}

			Instance = this;
			Initialized = true;
			DontDestroyOnLoad(gameObject);
		}

		private void SendCustomEvent(string eventName)
        {
            PrintEditorLog(eventName);
            UnityEngine.Analytics.Analytics.CustomEvent(eventName);
        }

        private void SendCustomEvent(string eventName, Dictionary<string, object> parameters)
        {
            PrintEditorLog(eventName, parameters);
            UnityEngine.Analytics.Analytics.CustomEvent(eventName, parameters);
        }

        private void PrintEditorLog(string eventName, Dictionary<string, object> parameters = null)
        {
            if (!analyticsConfig.ShowEditorLog)
			{
				return;
			}

#if UNITY_EDITOR
            StringBuilder sb = new StringBuilder();
            sb.Append("Send *");
            sb.Append(eventName);
            sb.Append("* event.");

            //If we have parameters, we set parameters in string
            if (parameters != null)
            {
                sb.Append(" Parameters {");
                foreach (KeyValuePair<string, object> parameter in parameters)
                {
                    sb.Append("( ");
                    sb.Append(parameter.Key);
                    sb.Append(",");
                    sb.Append(parameter.Value.ToString());
                    sb.Append(" ) ");
                }
                sb.Append("} ");
            }
            Debug.Log(sb.ToString().SetColor(Colors.cyan));
#endif
        }
        #endregion

        #region public_vars
        public static AnalyticsManager Instance
        {
            get
            {
                //If Instance doesnt exist, create and load config data from resources
                if (_instance == null)
                {
                    GameObject analyticsManager = new GameObject("AnalyticsManager");
					analyticsManager.SetActive(false);
                    _instance = analyticsManager.AddComponent<AnalyticsManager>();
                    _instance.analyticsConfig = Resources.Load(AnalyticsConfig.AssetName) as AnalyticsConfig;
                    _instance.Initialized = true;
                    analyticsManager.SetActive(true);

                    DontDestroyOnLoad(analyticsManager);
                }
                return _instance;
            }
            private set { _instance = value; }
        }
        #endregion

        #region private_vars
        [SerializeField]
        private AnalyticsConfig analyticsConfig;

        private static AnalyticsManager _instance;

		private bool Initialized = false;
		#endregion
	}
}