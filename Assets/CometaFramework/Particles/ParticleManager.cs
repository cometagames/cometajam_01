﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

public class ParticleManager : MonoBehaviour
{
    public static ParticleManager instance;
    private System.Action callbackAction;

    void Awake()
    {
        instance = this;
    }

    public GameObject GenerateParticle(string particleKey, Vector3 position)
    {
        GameObject particle;
        particle = PoolManager.Instance.GetPrefabPool(particleKey).Spawn();
        particle.transform.position = position;
        return particle;
    }

    public GameObject GenerateUIParticle(string particleKey, Vector3 position, float duration = 0f)
    {
        GameObject particle;
        RectTransform transformReference = GameObject.Find("CanvasUI")?.GetComponent<RectTransform>();//Encontrar un canvas dónde generar la partícula UI
        //Si no encuentra un canvas
        if (transformReference == null)
        {
            Canvas temporalCanvas = GetComponent<Canvas>();
            if (temporalCanvas == null)
            {
                temporalCanvas = gameObject.AddComponent<Canvas>();
            }
            temporalCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            temporalCanvas.worldCamera = Camera.main;
            transformReference = temporalCanvas.GetComponent<RectTransform>();
        }
        particle = PoolManager.Instance.Pools[particleKey].Spawn(position, Quaternion.identity);

        Vector3 finalWorldPosition = Vector3.zero;
        Vector2 screenPosition = RectTransformUtility.WorldToScreenPoint(Camera.main, position);
        RectTransformUtility.ScreenPointToWorldPointInRectangle(transformReference, screenPosition, Camera.main, out finalWorldPosition);

        particle.transform.SetParent(transformReference, false);
        particle.transform.position = finalWorldPosition;
        particle.SetActive(true);
        if (duration > 0)
        {
            if (particle.GetComponent<ParticleUI>() != null)
            {
                particle.GetComponent<ParticleUI>().DespawnByTime(duration);
            }
            else
            {
                Debug.LogError("Particle: " + particleKey + " has no ParticleUI component");
            }
        }
        return particle;
    }

    public void SpawnBunchOfUIParticles(int amount, string particleKey, Vector3 spawnPosition, System.Action OnComplete = null)
    {
        StartCoroutine(SpawnBunchOfUIParticlesCoroutine(amount, particleKey, spawnPosition, OnComplete));
    }

    IEnumerator SpawnBunchOfUIParticlesCoroutine(int amount, string particleKey, Vector3 position, System.Action OnComplete = null)
    {
        bool first = true;
        for (int i = 0; i < amount; i++)
        {
            Vector3 finalSpawnPosition = Random.insideUnitCircle;
            finalSpawnPosition += position;

            GameObject spawned = GenerateUIParticle(particleKey, finalSpawnPosition);
            ParticleUI particle = spawned.GetComponent<ParticleUI>();
            particle.ActivateParticle();
            //Only first particle, can send OnComplete callback
            if (first)
            {
                //Dependiendo del tipo de partícula, debería haber un sonido
                callbackAction = OnComplete;
                particle.FinishTween -= OnFinishFirstTween;
                particle.FinishTween += OnFinishFirstTween;
                first = false;
            }
            yield return null;
        }
    }

    private void OnFinishFirstTween(ParticleUI particle)
    {
        particle.FinishTween -= OnFinishFirstTween;
        callbackAction?.Invoke();
    }
}
