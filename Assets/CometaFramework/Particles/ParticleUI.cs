﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class ParticleUI : MonoBehaviour
{
    public float timeMoveAnimation = 0.5f;
    public float timeScaleAnimation = 0.5f;
    public float timeToDespawn = 1f;
    public bool speedBased;
    public Vector3 normalScale = Vector3.one;
    public string targetName;
    public Ease easePositionAnimation = Ease.InBack;
    public Ease easeScaleAnimation = Ease.InQuint;
    public float scaleToCenterDelay = 0.5f;
    public string poolKeyName;
    public delegate void OnFinishTween(ParticleUI particle);
    public OnFinishTween FinishTween;

    private Vector3 targetPosition;

    void OnEnable()
    {
        gameObject.transform.localScale = normalScale;
    }

    public void DespawnByTime(float time)
    {
        StartCoroutine(DespawnByTimeCoroutine(time));
    }

    IEnumerator DespawnByTimeCoroutine(float time)
    {
        yield return new WaitForSeconds(time);
        Despawn();
    }

    public void ActivateParticle()
    {
        if (!string.IsNullOrEmpty(targetName))
        {
            MoveToTarget();
        }
    }

    private void MoveToTarget()
    {
        gameObject.transform.localScale = normalScale;
        if (TargetTransform == null)
        {
            DespawnByTime(timeToDespawn);
            return;
        }
        targetPosition = TargetTransform.position;
        gameObject.transform.DOMove(targetPosition, timeMoveAnimation, false).SetEase(easePositionAnimation).SetSpeedBased(speedBased);
        gameObject.transform.DOScale(new Vector3(normalScale.x / 2, normalScale.y / 2, normalScale.z / 2), timeScaleAnimation).SetEase(easeScaleAnimation).SetSpeedBased(speedBased).OnComplete(Despawn);
    }

    private void Despawn()
    {
        gameObject.transform.localScale = normalScale;
        PoolManager.Instance.Pools[poolKeyName].Despawn(gameObject);
        FinishTween?.Invoke(this);
    }

    void OnDisable()
    {
        gameObject.transform.DOKill(false);
    }

    private Transform _targetTransform;
    public Transform TargetTransform
    {
        get
        {
            if (_targetTransform == null)
            {
                _targetTransform = GameObject.Find(targetName)?.transform;
            }
            return _targetTransform;
        }
    }
}
