﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ParticleManager))]
public class ParticleManagerEditor : Editor
{
    public string keyToTest = "UITest";
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ParticleManager particleManager = target as ParticleManager;


        if (GUILayout.Button("SPAWN BUCHCH OF PARTICLES"))
        {
            particleManager.SpawnBunchOfUIParticles(10, keyToTest, Vector3.zero);
        }
    }
}
