﻿using UnityEngine;
using UnityEngine.UI;

public class ParticleUIText : MonoBehaviour
{
    public string poolKey;
    public Text coinAmountText;
    public GameObject objectToDespawn;

    public void DespawnParticle()
    {
        PoolManager.Instance.Pools[poolKey].Despawn(objectToDespawn);
    }

    public void SetAmountText(int amount)
    {
        coinAmountText.text = amount.ToString();
    }
    public void SetText(string text)
    {
        coinAmountText.text = text;
    }
}
