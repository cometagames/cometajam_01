﻿using Papae.UnitySDK.Managers;
using UnityEngine;
using static MusicKeys;

namespace CometaFramework
{
    public class AudioController : MonoBehaviour
    {
        public static AudioController instance;

        private AudioManager audioManager;
        // reference to the audiosource for the loop clip
        private AudioSource loopSource = null;
        private MusicController musicController;

        void Awake()
        {
            instance = this;    
        }

        void Start()
        {
            musicController = GetComponent<MusicController>();
            audioManager = AudioManager.Instance;
        }

        public void SetMusicVolume(float value)
        {
            audioManager.MusicVolume = value;
        }

        public void SetSoundVolume(float value)
        {
            audioManager.SoundVolume = value;
        }

        public void ToggleMusic(bool value)
        {
            audioManager.IsMusicOn = value;
        }

        public void ToggleSound(bool value)
        {
            audioManager.IsSoundOn = value;
        }

        public void ToggleMaster(bool value)
        {
            audioManager.IsMasterMute = value;
        }

        public bool IsMusicOn()
        {
            return audioManager.IsMusicOn;
        }

        public bool IsSoundOn()
        {
            return audioManager.IsSoundOn;
        }

        public void ChangeMusic(string musicName, MusicTransition transitionType = MusicTransition.LinearFade, float transitionDuration = 0.5f)
        {
            audioManager.PlayBGM(GetAudioClipByName(musicName), transitionType, transitionDuration);
        }

        public void StopMusic()
        {
            audioManager.StopBGM();
        }

        public void PlayOneShotSoundEffect(string clipName, System.Action callbackFunction = null)
        {
            audioManager.PlayOneShot(GetAudioClipByName(clipName), callbackFunction);
        }

        public void PlayRepeatSoundEffect(string clipName, int timesToRepeat)
        {
            audioManager.RepeatSFX(GetAudioClipByName(clipName), timesToRepeat, true);
        }
        
        public void PlayLoopSoundEffect(string clipName, float loopDuration = 1f)
        {
            loopSource = audioManager.RepeatSFX(GetAudioClipByName(clipName), -1);
            Invoke("StopLoopingSoundEffect", loopDuration);
        }

        private void StopLoopingSoundEffect()
        {
            loopSource.Stop();
        }

        public void StopAllSFX()
        {
            audioManager.StopAllSFX();
        }

        public void ChangeMusicSnapShot(MusicID musicID)
        {
            musicController.PlayMusicSnapShot(musicID);
        }

        private AudioClip GetAudioClipByName(string clipName)
        {
            return audioManager.GetClipFromPlaylist(clipName);
        }
    }
}
