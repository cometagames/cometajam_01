﻿using UnityEngine;
using System;
using UnityEngine.Audio;
using System.Collections.Generic;
using static MusicKeys;

/// <summary>
/// Script que se usa cuando se quiere mútiples canciones de fondo sonando al mismo tiempo, pero sólo uno tiene el volumen activado
/// </summary>
namespace CometaFramework
{
    public class MusicController : MonoBehaviour
    {
        public float transitionTime = 0.5f;
        public List<MusicTrack> musicTracks;

        void Start()
        {
            CreateAudioSourceObjects();
        }

        private void CreateAudioSourceObjects()
        {
            foreach (MusicTrack track in musicTracks)
            {
                GameObject audioSourceObject = new GameObject();
                audioSourceObject.name = track.trackID + "AudioSource";
                audioSourceObject.transform.SetParent(transform);
                AudioSource snapShotObject = audioSourceObject.AddComponent<AudioSource>();
                snapShotObject.clip = track.audioClip;
                snapShotObject.outputAudioMixerGroup = track.mixerGroup;
                snapShotObject.playOnAwake = true;
                snapShotObject.loop = true;
            }
        }

        public void PlayMusicSnapShot(MusicID musicID)
        {
            musicTracks.Find(x => x.trackID == musicID).snapShot.TransitionTo(transitionTime);
        }
    }


    [Serializable]
    public class MusicTrack
    {
        public MusicID trackID;
        public AudioClip audioClip;
        public AudioMixerGroup mixerGroup;
        public AudioMixerSnapshot snapShot;
    }
}
