﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

namespace CometaFramework
{
    public static class AddressablesManager
    {
        public static AsyncOperationHandle<T> LoadAddressableAsset<T>(this AssetReference assetReference, System.Action<T> OnComplete) where T : Object
        {
            AsyncOperationHandle<T> asyncOperation = Addressables.LoadAssetAsync<T>(assetReference);
            if (OnComplete != null)
            {
                asyncOperation.Completed +=
                    (AsyncOperationHandle<T> obj) =>
                    {
                        if (obj.Status == AsyncOperationStatus.Succeeded) {
                            OnComplete.Invoke(obj.Result);
                        }
                        OnComplete.Invoke(null);
                    };
            }
            return asyncOperation;
        }

        public static AsyncOperationHandle<GameObject> InstantiateAddressable(this AssetReferenceGameObject assetReferenceGameObject,
            System.Action<GameObject> OnComplete, Transform container = null)
        {
            return assetReferenceGameObject.InstantiateAddressable(OnComplete, Vector3.zero, Quaternion.identity, container);
        }

        public static AsyncOperationHandle<GameObject> InstantiateAddressable(this AssetReferenceGameObject assetReferenceGameObject,
            System.Action<GameObject> OnComplete, Vector3 localPosition, Quaternion rotation, Transform container)
        {
            AsyncOperationHandle<GameObject> asyncOperation = assetReferenceGameObject.InstantiateAsync(localPosition, rotation, container);
            if (OnComplete != null)
            {
                asyncOperation.Completed +=
                    (AsyncOperationHandle<GameObject> obj) =>
                    {
                        if (obj.Status == AsyncOperationStatus.Succeeded)
                        {
                            GameObject loadedGO = obj.Result;
                            loadedGO.transform.localPosition = localPosition;
                            loadedGO.transform.localRotation = rotation;
                            OnComplete.Invoke(obj.Result);
                        }
                        OnComplete.Invoke(null);
                    };
            }
            return asyncOperation;
        }

        //Releases sprite and sprite renderer reference
        public static void Release(this SpriteRenderer spriteRenderer)
        {
            if (spriteRenderer.sprite != null)
            {
                Addressables.Release(spriteRenderer.sprite);
                spriteRenderer.sprite = null;
            }
        }

        public static void Release(this Image imageUI)
        {
            if (imageUI.sprite != null)
            {
                Addressables.Release(imageUI.sprite);
                imageUI.sprite = null;
            }
        }

        public static void Release<T>(this T asset) where T :Object
        {
            if (asset != null)
            {
                Addressables.Release(asset);
            }
        }

        public static void ReleaseInstance(this GameObject spawnedGameobject)
        {
            if (spawnedGameobject != null)
            {
                Addressables.ReleaseInstance(spawnedGameobject);
            }
        }
    }
}