﻿using CometaFramework;
using System.Collections;
using UnityEngine;

public class MissionsController : MonoBehaviour
{
    public MissionSO missionData;
    public static MissionsController instance;
    private Mission currentMission;

    public delegate void OnMissionTimerUpdate(int time);
    public event OnMissionTimerUpdate MissionTimerUpdate;

    public delegate void OnMissionUpdated();
    public event OnMissionUpdated MissionUpdated;

    public int GetMissionRemainingTime
    {
        get { return currentMission.remainingTime; }
    }

    public Mission CurrentMission
    {
        get { return currentMission; }
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        currentMission = SaveManager.Instance.Load(SaveKeys.Missions, new Mission());
        SetRemainingTime();
        MissionUpdated?.Invoke();
    }

    private void SetRemainingTime()
    {
        float offlineTime = OfflineTimeManager.instance.GetSecondsOffline();
        int remainingTime = currentMission.remainingTime - (int)offlineTime;
        if (remainingTime <= 0)
        {
            if (currentMission.subMissions.Count > 0)
            {
                if (currentMission.HasFinishedAllSubMissions() && currentMission.hasClaimedReward == false)
                {
                    currentMission.remainingTime = 0;
                    return;
                }
            }

            currentMission.ResetMission(missionData);
        }
        else
        {
            currentMission.remainingTime = remainingTime;
        }

        StopCoroutine("MissionTimerCoroutine");
        StartCoroutine("MissionTimerCoroutine");
    }

    IEnumerator MissionTimerCoroutine()
    {
        while (currentMission.remainingTime > 0)
        {
            yield return new WaitForSeconds(1);
            currentMission.remainingTime--;
            MissionTimerUpdate?.Invoke(currentMission.remainingTime);
        }

        if (!currentMission.HasFinishedAllSubMissions())
        {
            currentMission.ResetMission(missionData);
            MissionUpdated?.Invoke();
            StopCoroutine("MissionTimerCoroutine");
            StartCoroutine("MissionTimerCoroutine");
        }
    }

    public void UpdateMissions(MissionsTypes missionType)
    {
        currentMission.UpdateMissions(missionType);
        MissionUpdated?.Invoke();
    }

    public SubMission GetSubmissionWithTreshold()
    {
        return currentMission.HasSubmissionReachedTreshold();
    }

    public void ClaimMainMissionReward()
    {
        RewardObject rewardData = currentMission.ClaimReward();
        currentMission.ResetMission(missionData);
        MissionUpdated?.Invoke();
        StopCoroutine("MissionTimerCoroutine");
        StartCoroutine("MissionTimerCoroutine");
    }

    public void ClaimSubMissionReward(int subMissionIndex)
    {
        RewardObject rewardData = currentMission.subMissions[subMissionIndex].ClaimReward();
        Debug.Assert(rewardData.rewardType == RewardsType.Coins ||
                     rewardData.rewardType == RewardsType.Gems,
            "Cant add submission reward, if reward type is not coins or gems");
        //Entregar el premio en particula
        switch (rewardData.rewardType)
        {
            case RewardsType.Coins:
                //Spawnea monedas y las agrega al currency manager
                break;
            case RewardsType.Gems:
                //Spawnea gemas y las agrega al currency manager
                break;
        }
        //DiscoveryController.instance.ShowDiscoveryScreen(rewardData.rewardType, rewardData.rewardAmount, false);
        MissionUpdated?.Invoke();
    }


    public bool CanClaimSubMissionReward(int subMissionIndex)
    {
        if (currentMission.subMissions[subMissionIndex].IsSubMissionDone() &&
            !currentMission.subMissions[subMissionIndex].HasClaimedReward())
        {
            return true;
        }
        return false;
    }

    void OnApplicationPause(bool pause)
    {
        if (currentMission == null) return;
        if (!pause)
        {
            SetRemainingTime();
        }
        else
        {
            SaveManager.Instance.Save(SaveKeys.Missions, currentMission);
        }
    }

    public bool HasAnUnclaimedMission()
    {
        return currentMission.subMissions.Exists(x => x.IsSubMissionDone() && !x.HasClaimedReward());//Verificamos que exista alguna submisión terminada y que no se haya reclamado
    }

    public bool HasFinishedAllMissions()
    {
        return currentMission.HasFinishedAllSubMissions() && !currentMission.hasClaimedReward;
    }

    public int GetFinishedUnclaimedSubmissions()
    {
        return currentMission.GetFinishedUnclaimedSubMissions();
    }

    void OnApplicationQuit()
    {
#if UNITY_EDITOR
        SaveManager.Instance.Save(SaveKeys.Missions, currentMission);
#endif
    }

    void OnQuitGameEvent()
    {
        SaveManager.Instance.Save(SaveKeys.Missions, currentMission);
    }
}

