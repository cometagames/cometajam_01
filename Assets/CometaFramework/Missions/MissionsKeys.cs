﻿
namespace CometaFramework
{ 
    //Mission types
    public enum MissionsTypes
    {
        None,
        CollectThings,
        ReceiveObjects,
        AccelerateBatch
    }

    //Mission difficulties
    public enum MissionsDifficulty
    {
        None,
        Easy,
        Medium,
        Hard
    }

    public enum RewardsType
    {
        None,
        Coins,
        PuzzlePiece,
        Collectible,
        Gems
    }
}
