﻿using UnityEngine;
using CometaFramework;
using System;
using System.Collections.Generic;
using System.Linq;

[CreateAssetMenu(fileName = "DefaultMission", menuName = "Missions")]
public class MissionSO : ScriptableObject
{
    public int missionsDuration;
    public List<SubMission> availableSubmissions;
    public List<RewardsType> availableRewards;
    public Vector2 possibleGemsRewards;
    public static float[] submissionTresholds = new float[] { 30, 60, 100 };
}


[Serializable]
public class Mission
{
    public List<SubMission> subMissions;
    public bool hasClaimedReward;
    public int remainingTime;
    public RewardsType rewardType;
    public int rewardAmount;

    public Mission()
    {
        rewardType = RewardsType.Gems;
        rewardAmount = 1;
        hasClaimedReward = false;
        subMissions = new List<SubMission>();
        remainingTime = 0;
    }

    public int GetCompletedMissionsAmount()
    {
        return subMissions.FindAll(x => x.hasClaimedReward).Count;
    }

    public int GetFinishedUnclaimedSubMissions()
    {
        return subMissions.FindAll(x => x.IsSubMissionDone() && !x.HasClaimedReward()).Count;
    }

    public bool HasFinishedAllSubMissions()
    {
        return subMissions.All(x => x.hasClaimedReward == true);
    }

    public void UpdateMissions(MissionsTypes missionType)
    {
        foreach (SubMission subMission in subMissions.FindAll(x => x.missionType == missionType))
        {
            subMission.UpdateSubmission();
        }
    }

    public SubMission HasSubmissionReachedTreshold()
    {
        foreach (SubMission subMission in subMissions)
        {
            if (subMission.HasReachedTreshholds)
            {
                subMission.HasReachedTreshholds = false;
                return subMission;
            }
        }
        return null;
    }

    public RewardObject ClaimReward()
    {
        RewardObject rewardData = new RewardObject();
        hasClaimedReward = true;
        rewardData.rewardType = rewardType;
        rewardData.rewardAmount = rewardAmount;
        switch (rewardType)
        {
            case RewardsType.Gems:
                //Añadir recompensa
                break;
            case RewardsType.Coins:
                //Añadir recompensa
                break;
        }
        return rewardData;
    }

    public void ResetMission(MissionSO missionData)
    {
        hasClaimedReward = false;
        remainingTime = missionData.missionsDuration;
        subMissions.Clear();
        SetMissions(missionData);
        SetRewards(missionData);
    }

    private void SetRewards(MissionSO missionData)
    {
        int randomRewardIndex = UnityEngine.Random.Range(0, missionData.availableRewards.Count);
        rewardType = missionData.availableRewards[randomRewardIndex];
        switch (rewardType)
        {
            case RewardsType.Gems:
                rewardAmount = (int)UnityEngine.Random.Range(missionData.possibleGemsRewards.x, missionData.possibleGemsRewards.y);
                break;
            case RewardsType.PuzzlePiece:
                rewardAmount = 1;
                break;
        }
    }

    private void SetMissions(MissionSO missionData)
    {
        List<SubMission> temporalSubMissions = new List<SubMission>();

        foreach (SubMission subMission in missionData.availableSubmissions)
        {
            SubMission temporalSubMission = new SubMission();
            temporalSubMission.SetData(subMission.difficulty, subMission.missionType, subMission.goalRandomAmount, subMission.rewardType, subMission.rewardRandomAmount);
            temporalSubMissions.Add(temporalSubMission);
        }

        List<SubMission> easyMissions = temporalSubMissions.FindAll(x => x.difficulty == MissionsDifficulty.Easy).ToList();
        List<SubMission> normalMissions = temporalSubMissions.FindAll(x => x.difficulty == MissionsDifficulty.Medium).ToList();
        List<SubMission> hardMissions = temporalSubMissions.FindAll(x => x.difficulty == MissionsDifficulty.Hard).ToList();

        SubMission easyMission = SelectRandomMission(easyMissions);

        SubMission easyMissionOnNormal = normalMissions.SingleOrDefault(x => x.missionType == easyMission.missionType);//Obtenemos de la lista de misiones normales, la misión que ya se usó en las fáciles.
        normalMissions.Remove(easyMissionOnNormal);//La quitamos para que no se repita
        SubMission normalMission = SelectRandomMission(normalMissions);

        SubMission easyMissionOnHard = hardMissions.SingleOrDefault(x => x.missionType == easyMission.missionType);//Obtenemos de la lista de misiones difíciles, la misión que ya se usó en las fáciles.
        SubMission normalMissionOnHard = hardMissions.SingleOrDefault(x => x.missionType == normalMission.missionType);//Obtenemos de la lista de misiones difíciles, la misión que ya se usó en las normales.
        hardMissions.Remove(easyMissionOnHard);//La quitamos para que no se repita
        hardMissions.Remove(normalMissionOnHard);//La quitamos para que no se repita
        SubMission hardMission = SelectRandomMission(hardMissions);

        subMissions.Add(easyMission);
        subMissions.Add(normalMission);
        subMissions.Add(hardMission);
    }

    private SubMission SelectRandomMission(List<SubMission> subMissionList)
    {
        int selectedSubMissionIndex = UnityEngine.Random.Range(0, subMissionList.Count);
        return subMissionList[selectedSubMissionIndex];
    }
}


[Serializable]
public class SubMission
{
    public MissionsDifficulty difficulty;
    public MissionsTypes missionType;
    public RewardsType rewardType;
    public Vector2 goalRandomAmount;
    public Vector2 rewardRandomAmount;
    [HideInInspector]
    public int currentAmount;
    [HideInInspector]
    public bool hasClaimedReward;
    [HideInInspector]
    public int goalAmount;
    [HideInInspector]
    public int rewardAmount;
    [HideInInspector]
    public int currentTresholdIndex = -1;

    public bool HasReachedTreshholds
    {
        set;
        get;
    }

    public SubMission()
    {
        currentAmount = 0;
        hasClaimedReward = false;
    }

    public void SetData(MissionsDifficulty _difficulty, MissionsTypes _missionType, Vector2 _goalRandomAmount, RewardsType _rewardType, Vector2 _rewardAmount)
    {
        difficulty = _difficulty;
        missionType = _missionType;
        goalAmount = (int)UnityEngine.Random.Range(_goalRandomAmount.x, _goalRandomAmount.y);
        rewardType = _rewardType;
        rewardAmount = (int)UnityEngine.Random.Range(_rewardAmount.x, _rewardAmount.y);
        HasReachedTreshholds = false;
    }

    public bool IsSubMissionDone()
    {
        return currentAmount >= goalAmount;
    }

    public int CurrentAmount()
    {
        return currentAmount;
    }

    public bool HasClaimedReward()
    {
        return hasClaimedReward;
    }

    public void UpdateSubmission()
    {
        currentAmount++;
        if (currentAmount > goalAmount)
        {
            currentAmount = goalAmount;
        }

        float submissionPercentageDone = ((float)currentAmount / (float)goalAmount) * 100f;
        for (int i = MissionSO.submissionTresholds.Length - 1; i >= 0; i--)
        {
            if (submissionPercentageDone >= MissionSO.submissionTresholds[i])
            {
                if (currentTresholdIndex < i)
                {
                    currentTresholdIndex = i;
                    HasReachedTreshholds = true;
                }
            }
        }
    }

    public RewardObject ClaimReward()
    {
        RewardObject rewardData = new RewardObject();
        hasClaimedReward = true;
        rewardData.rewardType = rewardType;
        rewardData.rewardAmount = rewardAmount;
        return rewardData;
    }
}
