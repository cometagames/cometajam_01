﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public sealed class PoolManager : Singleton<PoolManager> 
{
	public bool initialized = false;
	/// <summary>
	/// List of child Prefab Pools GameObjects
	/// </summary>
	public List<PoolObject> m_PoolsPrefabs = new List<PoolObject>();
	/// <summary>
	/// Dictionary controling the references of all the PrefabPools, also the principal accesor to all pools
	/// </summary>
	public Dictionary<string,PrefabPool> Pools;

    public PrefabPool GetPrefabPool(string key)
    {
        Debug.Assert(Pools != null,"Pool manager is not initialized, we dont have a pool list");
        Debug.Assert(Pools.ContainsKey(key), "Pool manager doesnt contains key: "+key);
        return Pools[key];
    }

    public void DestroyObjectsOfPools()
	{
		for (int index = 0; index < Pools.Count; index++) 
		{
			var item = Pools.ElementAt(index);
			item.Value.DelteAllInternalObjects ();
//			var itemKey = item.Key;
//			var itemValue = item.Value;
		}
//		foreach(KeyValuePair<string,PrefabPool> data in Pools)
//		{
//			data.Value.DelteAllInternalObjects ();
//		}
	}
    public void Awake()
    {
        Initialize();
    }

    public void Initialize()
	{
		CreatePools();
		if(m_PoolsPrefabs!=null)
			if(m_PoolsPrefabs.Count>0)
		{
			PoolObject it = null;
			for(int i=0;i<m_PoolsPrefabs.Count;i++)
			{
				it = m_PoolsPrefabs[i];
				CreatePrefabPool(it.Prefab,it.PreloadedItems,it.name);
			}
		}
		initialized = true;
	}


	/// <summary>
	/// Creates a prefab pool and save the parent GameObject reference and the component into <see cref="Pools"/>.
	/// </summary>
	/// <param name="prefab">Prefab For the Pool to use.</param>
	/// <param name="amount">Amount of Preloaded Instances.</param>
	/// <param name="name">Name (optional) if not set, the name will be the prefab name.</param>
	public void CreatePrefabPool(GameObject prefab,int amount,string name)
	{
		CreatePools();
		if(string.IsNullOrEmpty(name))
		{
			Debug.Log("The pool name is empty or null");
			return;
		}
		if(Pools.ContainsKey(name))
		{
			Debug.Log("The pool name: "+name+" already exist");
			return;
		}
		if(name == null || name == string.Empty)
		{
			name = "Pool_"+prefab.name;
		}
		PrefabPool newpool = new PrefabPool("Pool_"+name,prefab,amount);
		Pools.Add(name,newpool);
		newpool.GetPoolParent().transform.parent = this.transform;
		//m_PoolsPrefabs.Add(newpool.GetPoolParent());
	}

	/// <summary>
	/// Initializer of the Dictionaries for the pools
	/// </summary>
	private void CreatePools()
	{
		if(Pools == null)
			Pools = new Dictionary<string, PrefabPool>();
		if(m_PoolsPrefabs == null)
			m_PoolsPrefabs = new List<PoolObject>();
	}

	public void CreatePrefabPoolInstances(string Key, int numInstances)
	{
		for(int i=0;i<numInstances;i++)
		{
			Pools[Key].CreateNewInstance();
		}
	}

	public void SetPausePrefabPool(string key, bool pause)
	{
		PrefabPool temp;
		if(Pools.TryGetValue(key,out temp))
		{
			temp.PausePrefabPoolObjects(pause);
		}
	}

	/// <summary>
	/// Pause or Unpause all prefab pools.
	/// </summary>
	/// <param name="active">If set to <c>true</c> active.</param>
	public void PauseAllPrefabPools(bool active)
	{
		foreach(KeyValuePair<string,PrefabPool> KVP in Pools)
		{
			Pools[KVP.Key].PausePrefabPoolObjects(active);
		}
	}

	/// <summary>
	/// Despawns all the pools.
	/// </summary>
	public void DespawnAllPools()
	{
		foreach(KeyValuePair<string,PrefabPool> KVP in Pools)
		{
			Pools[KVP.Key].DespawnAll();
		}
	}

}
