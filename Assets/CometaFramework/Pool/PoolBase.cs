﻿using UnityEngine;
using System.Collections.Generic;

public class PoolBase<T> {

	/// <summary>
	/// The _spawned T objects type list.
	/// </summary>
	protected List<T> _spawned;
	/// <summary>
	/// The _despawend T objects type list.
	/// </summary>
	protected List<T> _despawend;
	/// <summary>
	/// The amount of instances to preload.
	/// </summary>
	protected int m_iAmountInstances;

	/// <summary>
	/// Deletes all pool objects.
	/// </summary>
	public virtual void DelteAllInternalObjects(){}

	/// <summary>
	/// Creates the pool base.
	/// </summary>
	/// <param name="Amount">Amount to preload.</param>
	protected void CreatePoolBase(int Amount = 0)
	{
		if(m_iAmountInstances<0)
			throw new UnityException("Pool size invalid");
		_spawned = new List<T>();
		_despawend = new List<T>();
		m_iAmountInstances = Amount;
		//Populate();
	}

	/// <summary>
	/// Populate the pool with the number of instances preloaded.
	/// </summary>
//	private void Populate()
//	{
//		return;
//		for(int i=0;i<m_iAmountInstances;i++)
//		{
//			T clone = CreateInstance();
//		    _despawend.Add(clone);
//		}
//	}

	/// <summary>
	/// Returns the T object type available or creates a new one.
	/// </summary>
	public T Spawn()
	{
		//Blocking the memory segment for extra security.
		lock(_despawend)
		{
			if(_despawend.Count != 0)
			{
				//The pool contains at least one objects to spawn.
				T toSpawn = _despawend[0];
				_spawned.Add(toSpawn);
				_despawend.RemoveAt(0);
				OnSpawned(toSpawn);
				return toSpawn;
			}
			else
			{
				//The pool creates a new object to handle.
				T NewClone = CreateInstance();
				_spawned.Add(NewClone);
				OnSpawned(NewClone);
				return NewClone;
			}
		}
	}

	/// <summary>
	/// Despawn the T object type from the lists.
	/// </summary>
	/// <param name="instance">Instance to Despawn.</param>
	public void Despawn(T instance)
	{
		if(instance == null)
			return;
		//Blocking the memory segment for extra security.
		lock(_despawend)
		{
			if(_spawned.Contains(instance))
			{
				//The object exists in the pool and proceed to Despawn.
				_spawned.Remove(instance);
				_despawend.Add(instance);
				OnDespawned(instance);
			}
		}
	}

	/// <summary>
	/// Despawns all the objects of the selected pool.
	/// </summary>
	public void DespawnAll()
	{
		//Blocking the memory segment for extra security.
		lock(_despawend)
		{
			for(int i=0;i<_spawned.Count;i++)
			{
				T toDespawn = _spawned[i];
				_despawend.Add(toDespawn);
				OnDespawned(toDespawn);
			}
			_spawned.Clear();
		}
	}

	/// <summary>
	/// Special override function that allow to create the specific objetct type T for the pool to manage.
	/// </summary>
	/// <returns>The instance.</returns>
	protected virtual T CreateInstance()
	{
		return default(T);
	}

	/// <summary>
	/// Event occured just before the Spawn fucntion. Allow to perform actiones before returning the spawned object.
	/// </summary>
	/// <param name="clone">Spawned object.</param>
	protected virtual void OnSpawned(T clone) { }

	/// <summary>
	/// Event occured just before the Despawn fucntion. Allow to perform actiones before desspawning the object.
	/// </summary>
	protected virtual void OnDespawned(T clone) { }

}
