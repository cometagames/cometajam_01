﻿using UnityEngine;

/// <summary>
/// Manage all the functions from PoolBase to handle the objects as Object like Prefabs.
/// </summary>
public class PrefabPool : PoolBase<GameObject>
{
	/// <summary>
	/// The Prefab Object reference used for this pool
	/// </summary>
	protected GameObject prefab;
	/// <summary>
	/// The parent GameObject of the pool
	/// </summary>
	protected GameObject parent;

	/// <summary>
	/// Gets the GameObject Prefab used for the pool
	/// </summary>
	public GameObject GetPrefab()
	{
		return prefab;
	}

	public GameObject GetPoolParent()
	{
		return parent;
	}

	/// <summary>
	/// Deletes all pool objects.
	/// </summary>
	public override void DelteAllInternalObjects()
	{
		base.DelteAllInternalObjects();
		for (int i = 0; i < _spawned.Count; i++) 
		{
			Object.Destroy (_spawned[i]);
		}
//		foreach(Object data in _spawned)
//		{
//			Object.Destroy(data);
//		}
		_spawned.Clear();
		for (int i = 0; i < _despawend.Count; i++) 
		{
			Object.Destroy (_despawend[i]);
		}
//		foreach(Object data in _despawend)
//		{
//			Object.Destroy(data);
//		}
		_despawend.Clear();
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="PrefabPool"/> class.
	/// </summary>
	/// <param name="Prefab">Prefab to use.</param>
	/// <param name="amount">Preload Prefab Amount.</param>
	public PrefabPool(string name,GameObject Prefab, int amount) 
	{
		parent = new GameObject(name);
		//parent = Parent;
		prefab = Prefab;
		base.CreatePoolBase(amount);
	}

	/// <summary>
	/// Spawn in the specified pos and rot.
	/// </summary>
	/// <param name="pos">Spawn Position.</param>
	/// <param name="rot">Spawn Rotation.</param>
	public GameObject Spawn(Vector3 pos, Quaternion rot) 
	{
		GameObject spawned = base.Spawn();
		spawned.transform.position = pos;
		spawned.transform.rotation = rot;
		return spawned;
	}

	/// <summary>
	/// Event occured just before the Spawn fucntion. Allow to perform actiones before returning the spawned object.
	/// </summary>
	/// <param name="clone">Spawned object.</param>
	protected override void OnSpawned (GameObject clone)
	{
		base.OnSpawned (clone);
		clone.SetActive(true);
	}

	/// <summary>
	/// Event occured just before the Despawn fucntion. Allow to perform actiones before desspawning the object.
	/// </summary>
	/// <param name="clone">Clone.</param>
	protected override void OnDespawned (GameObject clone)
	{
		base.OnDespawned (clone);
		clone.SetActive(false);
	}

	/// <summary>
	/// Allow to create the specific objetct type Object for the pool to manage.
	/// </summary>
	/// <returns>The instance.</returns>
	protected override GameObject CreateInstance ()
	{
		GameObject t = base.CreateInstance ();
		t = UnityEngine.GameObject.Instantiate(prefab) as GameObject;
		t.SetActive(false);
        t.transform.SetParent(parent.transform);
		t.name = t.name+"_"+(_despawend.Count+_spawned.Count);
		return t;
	}

	public void CreateNewInstance()
	{
		GameObject t = CreateInstance();
		_despawend.Add(t);
		OnDespawned(t);
	}

	public void PausePrefabPoolObjects(bool pause)
	{
		for(int i=0;i<_spawned.Count;i++)
		{
			//(_spawned[i] as Transform).enabled=true;
			if(_spawned[i].GetComponent<Rigidbody2D>() != null)
				_spawned[i].GetComponent<Rigidbody2D>().velocity=Vector2.zero;
		}
	}

}
