﻿using UnityEngine;

[System.Serializable]
public class PoolObject{

	public string name = string.Empty;
	public GameObject Prefab = null;
	public int PreloadedItems = 1;
}
