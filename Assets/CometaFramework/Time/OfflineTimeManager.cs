﻿using UnityEngine;
using System.Collections;
using System;

public class OfflineTimeManager : MonoBehaviour
{
	public static OfflineTimeManager instance;

	//How much time has passed since the app was closed or suspended
	private double totalOffline = 0;
    //The time when we paused, it is represented as a string to keep the format

    private const string datePlayerPrefsKey = "Current_Date";


	void Awake()
	{
		instance = this;
	}

    void Start()
    {
        GetSecondsOffline();    
    }

    void OnApplicationQuit()
    {
        #if UNITY_EDITOR
        SaveDate();
        #endif
    }

    void OnQuitGameEvent()
    {
        SaveDate();
    }

    void OnApplicationPause(bool pause)
    {
        if(pause)
        {
            SaveDate();
        }
    }

    /// <summary>
    /// Substracts the current date, with the old date to get the seconds
    /// </summary>
    /// <returns>The seconds offline.</returns>
    public float GetSecondsOffline()
	{   
		DateTime currentDate = DateTime.Now;
        DateTime lastExitTime = LoadDateOnPlayerPrefs();//SaveManager.Instance.LoadOfflineDate();
	
		TimeSpan difference = currentDate.Subtract (lastExitTime);
		totalOffline = difference.TotalSeconds;
        if (totalOffline <= 0)
        {
            totalOffline = 0;
        }
		return (float)totalOffline;
	}

	public void SaveDate()
	{
        /*
        if (SaveManager.Instance != null)
        {
            SaveManager.Instance.SaveCurrentDate();
        }
        */
        SaveDateOnPlayerPrefs();
	}

    public bool HasPassedEnoughTime(float timeToCheck)
    {
        return (timeToCheck - (float)totalOffline <= 0) ? true : false;
    }

    //Métodos que no usan el Easy Save, porque no se ha implementado
    private void SaveDateOnPlayerPrefs()
    {
        PlayerPrefs.SetString(datePlayerPrefsKey, DateTime.Now.ToBinary().ToString());
    }

    private DateTime LoadDateOnPlayerPrefs()
    {
        long tempDate = Convert.ToInt64(PlayerPrefs.GetString(datePlayerPrefsKey));
        DateTime oldDate = DateTime.FromBinary(tempDate);
        return oldDate;
    }
}