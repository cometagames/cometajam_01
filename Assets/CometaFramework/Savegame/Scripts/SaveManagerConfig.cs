﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CometaFramework
{
    [CreateAssetMenu(menuName = "CometaFramework/SaveConfig", fileName = AssetName)]
    public class SaveManagerConfig : ScriptableObject
    {
        #region public_methods
        #endregion

        #region private_methods
        #endregion

        #region public_vars
        public string FileName
        {
            get
            {
                return filename + FILE_EXTENTION;
            }
        }

        public ES3Settings SaveSettings
        {
            get
            {
                if (_saveSettings == null)
                {
                    _saveSettings = new ES3Settings(ES3.EncryptionType.AES, password);
                }
                return _saveSettings;
            }
        }

        public string GetKeyString(SaveKeys key)
        {
            if (_dictionarySaveKeys == null)
            {
                _dictionarySaveKeys = new Dictionary<SaveKeys, string>();

                foreach (SaveKeys saveKey in System.Enum.GetValues(typeof(SaveKeys)))
                {
                    _dictionarySaveKeys.Add(saveKey, saveKey.ToString());
                }
            }
            return _dictionarySaveKeys[key];
        }

        public const string AssetName = "SaveConfig";
        #endregion

        #region private_vars
        [SerializeField]
        private string password = "MyPassword";
        [SerializeField]
        private string filename = "MyFileName";
        [SerializeField]
        private SaveKeys saveKeys;

        private ES3Settings _saveSettings;
        private Dictionary<SaveKeys, string> _dictionarySaveKeys;

        private const string FILE_EXTENTION = ".es3";
        #endregion
    }
}