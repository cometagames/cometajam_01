﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CometaFramework
{
    /// <summary>
    /// Import a SaveManager instance just in the first scene
    /// If SaveManager is not found it will be auto-created for testing purposes
    /// </summary>
    public class SaveManager : MonoBehaviour
    {
        #region public_methods
        public void Save<T>(SaveKeys saveKey, T value)
        {
            myFile.Save<T>(GetKeyString(saveKey), value);
        }

        public T Load<T>(SaveKeys saveKey, T defaultValue)
        {
            return myFile.Load(GetKeyString(saveKey), defaultValue);
        }

        public void DeleteKey(SaveKeys saveKey)
        {
            myFile.DeleteKey(GetKeyString(saveKey));
        }

        public void Sync()
        {
            myFile.Sync();
            ES3.CreateBackup(saveConfig.FileName, saveConfig.SaveSettings);
        }
        #endregion

        #region private_methods
        private void Awake()
        {
            if (Initialized)
            {
                return;
            }

            Instance = this;
            LoadFile();
            Initialized = true;
            DontDestroyOnLoad(gameObject);
        }

        private void LoadFile()
        {
            try
            {
                //First try normal load
                myFile = new ES3File(saveConfig.FileName, saveConfig.SaveSettings);
            }
            catch (System.Exception)
            {
                //If this didnt work, try to restore backup, and load file again
                if (ES3.RestoreBackup(saveConfig.FileName, saveConfig.SaveSettings))
                {
                    myFile = new ES3File(saveConfig.FileName, saveConfig.SaveSettings);
                }
            }
        }

        private string GetKeyString(SaveKeys saveKey)
        {
            return saveConfig.GetKeyString(saveKey);
        }
        #endregion

        #region public_vars
        public static SaveManager Instance
        {
            get
            {
                //If Instance doesnt exist, create and load config data from resources
                if (_instance == null)
                {
                    GameObject saveManager = new GameObject("SaveManager");
                    saveManager.SetActive(false);
                    _instance = saveManager.AddComponent<SaveManager>();
                    _instance.saveConfig = Resources.Load(SaveManagerConfig.AssetName) as SaveManagerConfig;
                    _instance.LoadFile();
                    _instance.Initialized = true;
                    saveManager.SetActive(true);

                    DontDestroyOnLoad(saveManager);
                }
                return _instance;
            }
            private set { _instance = value; }
        }
        #endregion

        #region private_vars
        [SerializeField]
        private SaveManagerConfig saveConfig;

        private ES3File myFile;

        private static SaveManager _instance;

        private bool Initialized = false;
        #endregion
    }
}