﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CometaFramework
{
    public class SplashscreenManager : MonoBehaviour
    {
        #region public_methods
        public void OnFinishSplashscreenAnimation()
        {
            Color temp = OverlayColor;
            temp.a = 0f;
            ScreenEffectsManager.Instance.overlayImage.color = temp;
            ScreenEffectsManager.Instance.OverlayFadeTo(1f, OverlayAnimationTime,
                () => {
                    LoadingSceneManager.IsWaitingEvent = false;
                });
        }
        #endregion

        #region private_methods
        private void Start()
        {
            LoadingSceneManager.LoadSceneWithLoadingScreen(GameSceneName, WaitForSplashscreenEvent);
        }
        #endregion

        #region public_vars

        #endregion

        #region private_vars
        [SerializeField]
        private string GameSceneName = "Game";
        [SerializeField]
        private bool WaitForSplashscreenEvent = true;

        [SerializeField]
        //Color to fade when splash animation is finished
        public Color OverlayColor = Color.black;
        [SerializeField]
        //Time to fade when splash animation is finished
        public float OverlayAnimationTime = 1f;
        #endregion
    }
}
