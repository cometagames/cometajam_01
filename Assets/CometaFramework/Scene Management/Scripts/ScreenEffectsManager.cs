﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace CometaFramework
{
    public class ScreenEffectsManager : MonoBehaviour
    {
        #region public_methods
        public void OverlayFadeTo(float endAlpha, float duration, System.Action OnComplete = null, float delay = 0.3f)
        {
            if (duration > 0)
            {
                overlayImage.DOFade(endAlpha, duration).SetDelay(delay).SetEase(Ease.Linear).OnComplete(() => { OnComplete?.Invoke(); });
            }
            else
            {
                Color tempColor = overlayImage.color;
                tempColor.a = endAlpha;
                overlayImage.color = tempColor;
                OnComplete?.Invoke();
            }
        }

        public void OverlayColorTo(Color color, float duration, System.Action OnComplete = null, float delay = 0.3f)
        {
            if (duration > 0)
            {
                overlayImage.DOColor(color, duration).SetDelay(delay).SetEase(Ease.Linear).OnComplete(() => { OnComplete?.Invoke(); });
            }
            else
            {
                overlayImage.color = color;
            }
        }
        #endregion

        #region private_methods
        private void Awake()
        {
            if (Initialized)
            {
                return;
            }

            Instance = this;

            Initialized = true;
            DontDestroyOnLoad(gameObject);
        }
        #endregion

        #region public_vars
        public static ScreenEffectsManager Instance
        {
            get
            {
                //If instance doesnt exist,create a new ScreenEffectsManager
                if (_instance == null)
                {
                    GameObject temp = new GameObject("TempHandler");
                    temp.SetActive(false);
                    GameObject prefab = Resources.Load(PREFAB_NAME) as GameObject;
                    GameObject screenFadeManager = Instantiate(prefab, temp.transform);
                    _instance = screenFadeManager.GetComponent<ScreenEffectsManager>();

                    _instance.Initialized = true;
                    screenFadeManager.transform.SetParent(null);
                    screenFadeManager.SetActive(true);
                    DontDestroyOnLoad(screenFadeManager);
                    Destroy(temp);
                }
                return _instance;
            }
            private set { _instance = value; }
        }

        public Image overlayImage;
        #endregion

        #region private_vars
        private static ScreenEffectsManager _instance;

        private bool Initialized = false;

        private const string PREFAB_NAME = "ScreenEffectsManager";
        #endregion
    }
}