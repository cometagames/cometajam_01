﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CometaFramework
{
    public class LoadingSceneManager : MonoBehaviour
    {
        #region public_methods
        public static void LoadSceneWithLoadingScreen(string sceneToLoad, bool waitingEvent)
        {
            GameObject loadingProcess = new GameObject("Loading LoadingScreen");
            loadingProcess.AddComponent<CoroutineComponent>().StartCoroutine(LoadSceneWithLoadingScreenCoroutine(sceneToLoad, waitingEvent));
        }

        public void OnFinishLoadingScreenAnimation()
        {
            ScreenEffectsManager.Instance.OverlayFadeTo(1f, OverlayAnimationTime,
                () => {
                    IsWaitingEvent = false;
                });
        }
        #endregion

        #region private_methods
        //Start loading with loading screen
        protected IEnumerator Start()
        {
            //Wait for last coroutine finish and reset LoadingScene boolean
            while (LoadingScene)
            {
                yield return null;
            }

            //First fade out camera overlay
            ScreenEffectsManager.Instance.OverlayFadeTo(0f, OverlayAnimationTime);

            WaitingEvent = WaitForEvent;

            LoadingScene = true;
            Scene previousScene = SceneManager.GetActiveScene();

            AsyncOperation async = SceneManager.LoadSceneAsync(SceneToLoad, LoadSceneMode.Additive);
            async.allowSceneActivation = false;

            if (progressBar != null)
            progressBar.fillAmount = 0f;

            while (WaitingEvent)
            {
                if (progressBar != null)
                    progressBar.fillAmount = Mathf.Clamp01(async.progress / 0.9f);
                yield return null;
            }

            async.allowSceneActivation = true;
            while (!async.isDone)
            {
                if (progressBar != null)
                    progressBar.fillAmount = Mathf.Clamp01(async.progress / 0.9f);
                yield return null;
            }

            Scene newScene = SceneManager.GetSceneByName(SceneToLoad);
            SceneManager.SetActiveScene(newScene);
            yield return null;
            SceneManager.UnloadSceneAsync(previousScene);
            LoadingScene = false;
        }

        //Loading before being in loading screen scene
        private static IEnumerator LoadSceneWithLoadingScreenCoroutine(string sceneToLoad, bool waitingEvent)
        {
            //Wait for last coroutine finish and reset LoadingScene boolean
            while (LoadingScene)
            {
                yield return null;
            }

            WaitingEvent = waitingEvent;
            SceneToLoad = sceneToLoad;

            LoadingScene = true;
            Scene previousScene = SceneManager.GetActiveScene();

            AsyncOperation async = SceneManager.LoadSceneAsync(LOADING_SCENE_NAME, LoadSceneMode.Additive);
            async.allowSceneActivation = false;

            while (WaitingEvent)
            {
                yield return null;
            }

            async.allowSceneActivation = true;
            while (!async.isDone)
            {
                yield return null;
            }

            Scene newScene = SceneManager.GetSceneByName(LOADING_SCENE_NAME);
            SceneManager.SetActiveScene(newScene);
            yield return null;
            SceneManager.UnloadSceneAsync(previousScene);
            LoadingScene = false;
        }
        #endregion

        #region public_vars
        public bool IsLoading
        {
            get
            {
                return LoadingScene;
            }
        }
        public static bool IsWaitingEvent
        {
            get
            {
                return WaitingEvent;
            }
            set
            {
                WaitingEvent = value;
            }
        }

        public const string LOADING_SCENE_NAME = "LoadingScreen";
        #endregion

        #region private_vars
        [SerializeField]
        private bool WaitForEvent = true;
        [SerializeField]
        private Image progressBar;
        [SerializeField]
        private float OverlayAnimationTime = 1f;

        private static bool WaitingEvent = false;
        private static string SceneToLoad = "";
        public static bool LoadingScene = false;
        #endregion
    }
}